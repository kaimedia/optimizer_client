const fs = require('fs');
const gulp = require('gulp');
const flatten = require('gulp-flatten');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cssmin = require('gulp-cssmin');
const sass = require('gulp-sass');
const replace = require('gulp-replace');

const vendorDir = './vendor';
const themeDir = './theme';
const font = { src: vendorDir + '/**/*.{ttf,woff,woff2,eot,svg}', dest: './src/assets/dist/fonts' };
const css = { src: './src/assets/sass/*.scss', dest: './src/assets/dist/css' };
const distDir = './src/assets/dist';

var vendorJs = [
    vendorDir + '/tether/dist/js/tether.min.js',
    themeDir + '/global/plugins/jquery.min.js',
    themeDir + '/global/plugins/jquery-migrate.min.js',
    themeDir + '/global/plugins/bootstrap/js/bootstrap.min.js',
    themeDir + '/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
    vendorDir + '/select2/dist/js/select2.min.js',
    themeDir + '/global/scripts/metronic.js',
    themeDir + '/admin/layout.js',
]

var vendorCss = [
    themeDir + '/global/plugins/bootstrap/css/bootstrap.min.css',
    themeDir + '/global/css/components.css',
]

gulp.task('vendorJs', () => {
    gulp.src(vendorJs)
        .pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(distDir + '/javascript'))
})

gulp.task('vendorCss', () => {
    gulp.src(vendorCss)
        .pipe(cssmin())
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(distDir + '/css'))
})

gulp.task('sass', function () {
    gulp.src(font.src)
        .pipe(flatten())
        .pipe(gulp.dest(font.dest));
    gulp.src(css.src)
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(gulp.dest(css.dest));
});

gulp.task('clean-client-cache', function () {
    const version = new Date().getTime();
    return gulp.src('./src/index.html')
        .pipe(replace(/\/assets\/dist\/css\/app.css/g, `/assets/dist/css/app.css?v=${version}`))
        .pipe(replace(/\/assets\/dist\/css\/vendor.css/g, `/assets/dist/css/vendor.css?v=${version}`))
        .pipe(replace(/\/assets\/dist\/javascript\/vendor.js/g, `/assets/dist/javascript/vendor.js?v=${version}`))
        .pipe(gulp.dest('./src/'));
});

gulp.task('dev', ['vendorJs', 'vendorCss', 'sass']);
gulp.task('prod', ['vendorJs', 'vendorCss', 'sass', 'clean-client-cache']);