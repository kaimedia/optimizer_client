const config = require('./config.json');

export const environment = {
  production: true,
  API_PATH: "https://theoptimizer.herokuapp.com/api",
  DEFAULT_MANAGER: config.DEFAULT_MANAGER,
  refreshTime: config.refreshTime,
  timeZone: config.timeZone
};
