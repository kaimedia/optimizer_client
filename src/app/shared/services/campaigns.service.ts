import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';
import { Campaign } from '../../models/campaign';

@Injectable()
class CampaignService {
    constructor(
        private apiConnector: APIConnector,
    ) { }

    list(dateRange, cache = 1): Observable<any> {
        return this.apiConnector.requestAPI('get', `/campaign/list?start_date=${dateRange.start_date}&end_date=${dateRange.end_date}&cache=${cache}`);
    }

    detail(campaignId): Observable<any> {
        return this.apiConnector.requestAPI('get', `/campaign/detail?campaign_id=${campaignId}`);
    }

    campaignApiUpdate(data): Observable<any> {
        return this.apiConnector.requestAPI('post', '/campaign/campaignApiUpdate', data);
    }
    allTotals(dateRange): Observable<any> {
        return this.apiConnector.requestAPI('get', `/campaign/getTotals?start_date=${dateRange.start_date}&end_date=${dateRange.end_date}`, dateRange);
    }

    itemTotals(data): Observable<any> {
        return this.apiConnector.requestAPI('get', `/campaign/getTotalsDetail?id=${data.campaign_id}&start_date=${data.start_date}&end_date=${data.end_date}`);
    }

    status(campaign): Observable<any> {
        return this.apiConnector.requestAPI('patch', '/campaign/changeStatus', campaign);
    }

    changeBid(data): Observable<any> {
        return this.apiConnector.requestAPI('patch', '/campaign/changeBid', data);
    }

    changeSiteCpc(site): Observable<any> {
        return this.apiConnector.requestAPI('patch', `/campaign/changeSiteCpc`, site);
    }

    fraudStatsCampaign(campaign_id, range): Observable<any> {
        return this.apiConnector.requestAPI('get', `/campaign/fraudStatsDaily?id=${campaign_id}&start_date=${range.start_date}&end_date=${range.end_date}`);
    }

    fraudTrackingCampaign(campaign_id, range): Observable<any> {
        return this.apiConnector.requestAPI('get', `/campaign/fraudStatsParams?id=${campaign_id}&start_date=${range.start_date}&end_date=${range.end_date}`);
    }

    //db
    getAllCampaigns(): Observable<Array<Campaign>> {
        return this.apiConnector.requestAPI('get', '/campaign/all');
    }

    getArchives(): Observable<Array<Campaign>> {
        return this.apiConnector.requestAPI('get', '/campaign/archiveList');
    }

    getAllByStatus(status): Observable<Array<Campaign>> {
        return this.apiConnector.requestAPI('get', `/campaign/allByStatus?status=${status}`);
    }

    create(data): Observable<Campaign> {
        return this.apiConnector.requestAPI('post', '/campaign/create', data);
    }

    update(data): Observable<Campaign> {
        return this.apiConnector.requestAPI('post', '/campaign/update', data);
    }

    archive(campaignId): Observable<any> {
        return this.apiConnector.requestAPI('post', `/campaign/archive?id=${campaignId}`);
    }

    unArchive(campaignId): Observable<any> {
        return this.apiConnector.requestAPI('post', `/campaign/unArchive?id=${campaignId}`);
    }

    campaignMetrics(type, dateRange): Observable<any> {
        return this.apiConnector.requestAPI('get', `/campaign/campaignMetrics?type=${type}&start_date=${dateRange.start_date}&end_date=${dateRange.end_date}`);
    }

    updateStats(campaign_id: string, start_date: string, end_date: string): Observable<any> {
        return this.apiConnector.requestAPI('get', `/campaign/updateStats?campaign_id=${campaign_id}&start_date=${start_date}&end_date=${end_date}`);
    }
}

export { CampaignService, Campaign }