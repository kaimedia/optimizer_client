import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class VoluumService {
    constructor(
        private apiConnector: APIConnector,
    ) { }

    trackerView(tracker_id: string, dateRange: any): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/tracker-view?tracker=${tracker_id}&start_date=${dateRange.start_date}&end_date=${dateRange.end_date}`);
    }

    getTrafficSources(tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/traffic-sources?tracker_id=${tracker_id}`);
    }

    getAffiliateNetworks(tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/affiliate-network?tracker_id=${tracker_id}`);
    }

    //campaign
    getCampaignDetail(campaignId): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/campaign?id=${campaignId}`);
    }

    createCampaign(campaignId, payload): Observable<any> {
        return this.apiConnector.requestAPI('post', `/voluum/campaign?campaign_id=${campaignId}`, payload);
    }

    updateCampaign(campaignId, payload): Observable<any> {
        return this.apiConnector.requestAPI('put', `/voluum/campaign?id=${campaignId}`, payload);
    }

    //lander
    getLanders(tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/landers?tracker_id=${tracker_id}`);
    }

    getLander(landerId, tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/lander?id=${landerId}&tracker_id=${tracker_id}`);
    }

    createLander(payload, tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('post', `/voluum/lander?tracker_id=${tracker_id}`, payload);
    }

    updateLander(landerId, payload, tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('put', `/voluum/lander?id=${landerId}&tracker_id=${tracker_id}`, payload);
    }

    //offer
    getOffers(tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/offers?tracker_id=${tracker_id}`);
    }

    getOffer(offerId, tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/offer?id=${offerId}&tracker_id=${tracker_id}`);
    }

    createOffer(payload, tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('post', `/voluum/offer?tracker_id=${tracker_id}`, payload);
    }

    updateOffer(offerId, payload, tracker_id = ''): Observable<any> {
        return this.apiConnector.requestAPI('put', `/voluum/offer?id=${offerId}&tracker_id=${tracker_id}`, payload);
    }

    //tracker account
    trafficSources(trackerId): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/trafficSourcesByTracker?tracker_id=${trackerId}`);
    }

    getLandersByTracker(trackerId): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/landersByTracker?tracker_id=${trackerId}`);
    }

    getOffersByTracker(trackerId): Observable<any> {
        return this.apiConnector.requestAPI('get', `/voluum/offersByTracker?tracker_id=${trackerId}`);
    }
}
