import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class ContentService {
    constructor(
        private apiConnector: APIConnector,
    ) { }

    list(campaign_id, dateRange, cache = 1): Observable<any> {
        return this.apiConnector.requestAPI('get', `/content/list?campaign_id=${campaign_id}&start_date=${dateRange.start_date}&end_date=${dateRange.end_date}&cache=${cache}`);
    }

    update(campaign_id, item_id, data): Observable<any> {
        return this.apiConnector.requestAPI('post', `/content/update?campaign_id=${campaign_id}&item_id=${item_id}`, data);
    }

    remove(campaign_id, item_id) {
        return this.apiConnector.requestAPI('delete', `/content?campaign_id=${campaign_id}&item_id=${item_id}`);
    }

    contentMetrics(campaign_id, dateRange): Observable<any> {
        return this.apiConnector.requestAPI('get', `/content/contentMetrics?campaign_id=${campaign_id}&start_date=${dateRange.start_date}&end_date=${dateRange.end_date}`);
    }

    updateStats(campaign_id: string, start_date: string, end_date: string): Observable<any> {
        return this.apiConnector.requestAPI('get', `/content/updateStats?campaign_id=${campaign_id}&start_date=${start_date}&end_date=${end_date}`);
    }
}
