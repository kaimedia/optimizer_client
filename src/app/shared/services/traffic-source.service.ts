import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';
import { TrafficSource } from '../../models/traffic-source';

@Injectable()
class TrafficSourceService {
    constructor(
        private apiConnector: APIConnector,
    ) { }

    list(): Observable<any> {
        return this.apiConnector.requestAPI('get', '/traffic');
    }

    create(trafficSource) {
        return this.apiConnector.requestAPI('post', '/traffic', trafficSource);
    }

    get(trafficSourceId): Observable<TrafficSource> {
        return this.apiConnector.requestAPI('get', `/traffic/${trafficSourceId}`);
    }

    update(trafficSourceId, data): Observable<any> {
        data.updated_at = new Date();
        return this.apiConnector.requestAPI('patch', `/traffic/${trafficSourceId}`, data)
    }

    remove(trafficSourceId) {
        return this.apiConnector.requestAPI('delete', `/traffic/${trafficSourceId}`);
    }
}

export { TrafficSourceService, TrafficSource }
