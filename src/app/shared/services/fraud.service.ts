import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class FraudService {
    constructor(
        private apiConnector: APIConnector,
    ) { }

    list(): Observable<any> {
        return this.apiConnector.requestAPI('get', `/fraud/campaign/list`);
    }

    statsAll(range): Observable<any> {
        return this.apiConnector.requestAPI('get', `/fraud/statsTotals?start_date=${range.start_date}&end_date=${range.end_date}`);
    }
}
