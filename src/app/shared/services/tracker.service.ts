import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';
import { Tracker } from '../../models/tracker';

@Injectable()
class TrackerService {
    constructor(
        private apiConnector: APIConnector,
    ) { }

    list(): Observable<any> {
        return this.apiConnector.requestAPI('get', '/tracker');
    }

    create(tracker) {
        return this.apiConnector.requestAPI('post', '/tracker', tracker);
    }

    get(trackerId): Observable<Tracker> {
        return this.apiConnector.requestAPI('get', `/tracker/${trackerId}`);
    }

    update(trackerId, data): Observable<any> {
        data.updated_at = new Date();
        return this.apiConnector.requestAPI('patch', `/tracker/${trackerId}`, data)
    }

    remove(trackerId) {
        return this.apiConnector.requestAPI('delete', `/tracker/${trackerId}`);
    }
}

export { TrackerService, Tracker }
