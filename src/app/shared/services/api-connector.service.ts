import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { TokenService } from './token.service';
import { AccountsService } from './accounts.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class APIConnector {
    constructor(
        private router: Router,
        private http: Http,
        private tokenService: TokenService,
        private accountsService: AccountsService,
    ) { }

    requestAPI(method: string, _uri: string, _data: Object = null): Observable<any> {
        const API_PATH = environment.API_PATH;
        const token: string = this.tokenService.getToken();
        const accountAPI = this.accountsService.get();

        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "Bearer " + token);
        
        if (method.toLowerCase() === 'delete') {
            headers.delete('Transfer-Encoding');
        }

        if (accountAPI) {
            headers.append('Traffic', accountAPI.traffic_source.id);
            headers.append('Tracker', accountAPI.tracker.id);
        }

        let uri = _uri;
        let data = _data;

        const options = new RequestOptions({ headers: headers });
        if (method.toLowerCase() === 'get' || method.toLowerCase() === 'delete') {
            if (data) {
                uri += '?';
                for (var key in data) {
                    if (data[key])
                        uri += `${key}=${data[key]}&`;
                }
                uri = uri.slice(0, -1);
            }
            data = null;
        } else if (!data) {
            data = {};
        }

        const request: Observable<any> = !data ? this.http[method](API_PATH + uri, options) : this.http[method](API_PATH + uri, data, options);
        return request.map(res => this.completeRequest(res, 'extractData')).catch(err => this.completeRequest(err, 'handleError'));
    }

    private completeRequest(res, callback) {
        return this[callback](res);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    public handleError(error: any) {
        if (error.status === 401 || error.statusText === 'Unauthorized') {
            this.reLogin();
        }

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body || JSON.stringify(body);
            errMsg = err.message ? err.message : err.description ? err.description : 'Server Maintenance';
        } else {
            errMsg = 'Something went wrong!';
        }
        return Observable.throw(errMsg);
    }

    reLogin() {
        this.tokenService.logOut();
        this.router.navigateByUrl('/auth/login');
    }

}