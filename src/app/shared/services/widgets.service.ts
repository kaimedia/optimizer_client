import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';
import { Site } from '../../models/site';

@Injectable()
class WidgetService {
    constructor(
        private apiConnector: APIConnector,
    ) { }

    list(data: any = null, status: string = 'active', cache: boolean = true): Observable<any> {
        return this.apiConnector.requestAPI('get', `/site/list?campaign_id=${data.campaign_id}&status=${status}&start_date=${data.start_date}&end_date=${data.end_date}&cache=${cache}`);
    }

    blockSites(campaignId: any, sites: any): Observable<any> {
        return this.apiConnector.requestAPI('patch', `/site/blockSites?campaign_id=${campaignId}`, sites);
    }

    unBlockSites(campaignId: any, sites: any): Observable<any> {
        return this.apiConnector.requestAPI('patch', `/site/unBlockSites?campaign_id=${campaignId}`, sites);
    }

    status(sites: any, status: string): Observable<any> {
        return this.apiConnector.requestAPI('patch', `/site/changeStatus?status=${status}`, sites);
    }

    getStatus(status: string): Observable<any> {
        return this.apiConnector.requestAPI('get', `/site/getStatus?status=${status}`);
    }

    getBlacklist(trafficSource: string, status: string): Observable<Array<Site>> {
        return this.apiConnector.requestAPI('get', `/blacklist/list?traffic_source=${trafficSource}&status=${status}`);
    }

    manualUnBlockSite(sites: any): Observable<any> {
        return this.apiConnector.requestAPI('patch', `/blacklist/unBlock`, sites);
    }

    statusAutoChangeCpc(site: any, status: boolean): Observable<any> {
        return this.apiConnector.requestAPI('post', `/site/changeAutoCpc?status=${status}`, site);
    }

    getBlockedSites(trafficSource: string): Observable<any> {
        return this.apiConnector.requestAPI('get', `/site/blockedSites?traffic_source=${trafficSource}`);
    }

    siteMetricsByCampaign(data: any = null): Observable<any> {
        return this.apiConnector.requestAPI('get', `/site/siteMetrics?campaign_id=${data.campaign_id}&start_date=${data.start_date}&end_date=${data.end_date}`);
    }

    updateStats(campaign_id: string, start_date: string, end_date: string): Observable<any> {
        return this.apiConnector.requestAPI('get', `/site/updateStats?campaign_id=${campaign_id}&start_date=${start_date}&end_date=${end_date}`);
    }
}

export { WidgetService, Site }