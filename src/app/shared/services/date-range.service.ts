import { Injectable } from '@angular/core';
import { StorageService } from "../../core/services/storage.service";

@Injectable()
export class DateRangeService {
    dateRange: Object;
    name: string = 'date_range';

    constructor(
        private storageService: StorageService,
    ) { }

    set(dateRange): void {
        this.storageService.setObject(this.name, dateRange);
    }

    get() {
        return this.storageService.getObject(this.name);
    }

    remove() {
        this.storageService.remove(this.name);
    }
}