import { Injectable } from '@angular/core';
import { Router, CanActivate, CanDeactivate, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { StorageService } from "../../core/services/storage.service";

const key = 'accounts';

@Injectable()
export class AccountsService {
    accounts: Object;

    constructor(
        private storageService: StorageService,
    ) { }

    set(accounts): void {
        this.storageService.setObject(key, accounts);
    }

    get() {
        return this.storageService.getObject(key);
    }

    remove() {
        this.storageService.remove(key);
    }
}

@Injectable()
export class AccountGuardService implements CanActivate {
    constructor(
        private router: Router,
        private storageService: StorageService,
    ) {

    }
    canActivate(): Observable<boolean> | boolean {
        if (this.isLinkedAccount())
            return true;

        this.router.navigate(['/dashboard']);

        return false;
    }

    isLinkedAccount() {
        let account = this.storageService.getObject(key);
        if (account)
            return true;

        return false;
    }

}