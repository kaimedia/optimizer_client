import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';
import { Rule } from '../../models/rule';

import * as _ from 'lodash';

@Injectable()
class RuleService {
    constructor(
        private apiConnector: APIConnector,
    ) { }

    list(): Observable<any> {
        return this.apiConnector.requestAPI('get', '/rule');
    }

    create(rule) {
        return this.apiConnector.requestAPI('post', '/rule', rule);
    }

    get(ruleId): Observable<Rule> {
        return this.apiConnector.requestAPI('get', `/rule/${ruleId}`);
    }

    update(ruleId, data): Observable<any> {
        data.updated_at = new Date();
        return this.apiConnector.requestAPI('patch', `/rule/${ruleId}`, data)
    }

    remove(ruleId) {
        return this.apiConnector.requestAPI('delete', `/rule/${ruleId}`);
    }
}

export { RuleService, Rule }
