import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { User } from '../../models/user';
import { APIConnector } from '../services/api-connector.service';
import { TokenService } from './token.service';
import { StorageService } from "../../core/services/storage.service";

@Injectable()
class UserService {
    user: User;
    constructor(
        private router: Router,
        private apiConnector: APIConnector,
        private tokenService: TokenService,
        private storageService: StorageService,
    ) { }

    login(user, callback) {
        this.apiConnector.requestAPI('post', '/auth/login', user).subscribe(response => {
            this.tokenService.setToken(response.token);
            callback(null, response);
        }, err => {
            callback(err);
        })
    }

    register(user, callback) {
        this.apiConnector.requestAPI('post', '/auth/register', user).subscribe(response => {
            callback(null, response);
        }, err => {
            callback(err);
        })
    }

    logout() {
        this.user = undefined;
        this.tokenService.logOut();
        const returnUrl = this.storageService.get('return-url');
        if (returnUrl) {
            this.storageService.remove('return-url');
            window.location.href = returnUrl;
        }
        else {
            this.router.navigate(['/auth/login']);
        }
    }

    get(userId = null): Observable<User> {
        if (this.user) {
            return Observable.of(this.user);
        }
        let apiUrl = '/auth/me';
        if (userId) {
            apiUrl = `/user/${userId}`;
        }

        return this.apiConnector.requestAPI('get', apiUrl).map(user => {
            if (user) {
                this.user = user;
                this.tokenService.setUserId(user._id);
            }
            return user;
        }).catch((err: any) => {
            this.tokenService.logOut();
            if (err.status === 401 || err.status === 404) {
                this.router.navigate(['/error/401']);
            } else {
                this.router.navigate(['/error']);
            }
            return Observable.of(null);
        });
    }

    list(): Observable<any> {
        return this.apiConnector.requestAPI('get', '/user');
    }

    update(user): Observable<User> {
        return this.apiConnector.requestAPI('patch', '/user', user);
    }

    changePassword(user): Observable<User> {
        return this.apiConnector.requestAPI('patch', '/user/password', user);
    }

    getManagerInfo(managerId): Observable<any> {
        return this.apiConnector.requestAPI('get', `/user/${managerId}`);
    }
}

export { UserService, User }