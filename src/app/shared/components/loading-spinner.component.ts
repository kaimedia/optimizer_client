import { Component, OnInit, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import { PageLoadingService } from '../services/page-loading.service';
import { Subscription } from 'rxjs';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';

@Component({
    selector: 'loading-spinner',
    template: `<span *ngIf="showLoading"
            [class]="'loading-spiner '+color"
            [style.width]="size+'px'"
            [style.height]="size+'px'"
            [style.border-width]="border+'px'"
            [style.display]="display"
            [style.margin]="margin"
            [style.position]="absolute?'absolute':'relative'"
            [style.right]="fixed=='right'?'0':'auto'"
            [style.left]="fixed=='left'?'0':'auto'"
            [style.top]="'50%'"
            ></span>`,
    styles: [
        '.loading-spiner.white{border-color: rgba(255,255,255,.2);border-top-color: #fff;}'
    ]
})
export class LoadingSpinnerComponent implements OnInit, OnDestroy {
    @Input() size: number = 50;
    @Input() border: number = 8;
    @Input() color: string = 'default';
    @Input() display: string = 'block';
    @Input() margin: string = 'auto';
    @Input() absolute: boolean = false;
    @Input() fixed: string = 'right';

    showLoading: boolean = false;
    intervalId: any;
    eventSub: Subscription;
    pageLoad: boolean = false;

    constructor(
        private loadingService: PageLoadingService,
        private _cdRef: ChangeDetectorRef,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.router.events.subscribe(e => {
            if (e instanceof NavigationStart) {
                this.pageLoad = true;
            } else if (e instanceof NavigationEnd || e instanceof NavigationCancel || e instanceof NavigationError) {
                this.pageLoad = false;
            }
        })
        this.eventSub = this.loadingService.eventLoading.subscribe(e => {
            if (e && !this.pageLoad) {
                this[e]();
            }
            this._cdRef.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.eventSub.unsubscribe();
    }

    private start(): void {
        this.showLoading = true;
    }

    private complete(): void {
        this.showLoading = false;
    }
}