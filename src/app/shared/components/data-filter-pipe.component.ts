import { Pipe, PipeTransform } from "@angular/core";
import * as _ from "lodash";

@Pipe({
    name: "filter",
    pure: false
})

export class DataFilterPipe implements PipeTransform {
    transform(array: any[], field: string, query: string): any {
        if (query) {
            return _.filter(array, row => {
                if (row[field]) {
                    return _.toLower(row[field]).indexOf(_.toLower(query)) > -1;
                }
            });
        }
        return array;
    }
}