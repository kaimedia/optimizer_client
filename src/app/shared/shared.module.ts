import { NgModule } from '@angular/core';
import { BrowserXhr } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ModalModule } from 'ngx-bootstrap';
import { LoadingXhrProvider } from './providers/loading-xhr.provider';

import { UserResolver } from './resolves/user.resolver';

import { UserService } from './services/user.service';
import { APIConnector } from './services/api-connector.service';
import { CampaignService } from './services/campaigns.service';
import { RoleService } from './services/role.service';
import { RuleService } from './services/rule.service';
import { WidgetService } from './services/widgets.service';
import { ContentService } from './services/content.service';
import { TrackerService } from './services/tracker.service';
import { TrafficSourceService } from './services/traffic-source.service';
import { FraudService } from './services/fraud.service';
import { DateRangeService } from "./services/date-range.service";
import { TokenService } from './services/token.service';
import { AccountsService, AccountGuardService } from './services/accounts.service';
import { AuthGuardService } from './services/auth-guard.service';
import { UnAuthGuardService } from './services/un-auth-guard.service';
import { VoluumService } from './services/voluum.service';

import { PageLoadingComponent } from './components/page-loading.component';
import { LoadingSpinnerComponent } from './components/loading-spinner.component';
import { HttpLoadingComponent } from './components/http-loading.component';
import { MessageFlashComponent } from './components/message-flash.component';
import { ValidateMessage } from './components/validate-message.component';
import { ErrorHandlerComponent } from './components/error-handler.component';
import { SelectCountryComponent } from './components/select-country.component';
import { SelectCountryCodeComponent } from './components/select-country-code.component';

@NgModule({
    declarations: [
        PageLoadingComponent,
        LoadingSpinnerComponent,
        HttpLoadingComponent,
        MessageFlashComponent,
        ValidateMessage,
        ErrorHandlerComponent,
        SelectCountryComponent,
        SelectCountryCodeComponent,
    ],
    imports: [
        FormsModule,
        CommonModule,
        NgxPermissionsModule.forRoot(),
        ModalModule.forRoot(),
    ],
    exports: [
        NgxPermissionsModule,
        PageLoadingComponent,
        LoadingSpinnerComponent,
        HttpLoadingComponent,
        MessageFlashComponent,
        ValidateMessage,
        ErrorHandlerComponent,
        SelectCountryComponent,
        SelectCountryCodeComponent,
    ],
    providers: [
        {
            provide: BrowserXhr, useClass: LoadingXhrProvider
        },
        RoleService,
        UserService,
        APIConnector,
        CampaignService,
        WidgetService,
        ContentService,
        FraudService,
        VoluumService,
        TrackerService,
        TrafficSourceService,
        DateRangeService,
        RuleService,
        TokenService,
        AccountsService,
        AccountGuardService,
        AuthGuardService,
        UnAuthGuardService,
        UserResolver,
    ],
})

export class SharedModule { }