import { Component, Input, OnInit } from '@angular/core';
import { UserService, User } from '../../shared/services/user.service';

@Component({
    moduleId: module.id,
    selector: 'app-user',
    templateUrl: './templates/user.component.html',
})
export class UserComponent implements OnInit {
    constructor(private userService: UserService) { }

    ngOnInit() { }
}