import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService, User } from '../../shared/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { RegexHelper } from '../../shared/helpers/regex.helper';

@Component({
    moduleId: module.id,
    selector: 'user-profile',
    templateUrl: './templates/profile.component.html',
})
export class ProfileComponent implements OnInit, OnDestroy {
    user: User;
    subUser: any;
    regexHelper = RegexHelper;
    
    constructor(
        private userService: UserService,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageFlashService
    ) { }

    ngOnInit() {
        this.subUser = this.activatedRoute.parent.parent.parent.data.subscribe((data: { user: User }) => {
            this.user = data.user;
        });
    }

    ngOnDestroy() {
        this.subUser.unsubscribe();
    }

    onSubmit() {
        if (!this.canSubmit()) {
            return false;
        }

        const userData = {
            _id: this.user._id,
            firstName: this.user.firstName,
            lastName: this.user.lastName,
            email: this.user.email,
            address: this.user.address,
            country: this.user.country,
            updated_at: new Date()
        };

        this.userService.update(userData).subscribe(
            response => {
                this.userService.user = this.user;
                this.messageService.flashSuccess('Updated successfully');
            }, err => {
                this.messageService.flashError(err);
            });
    }

    canSubmit() {
        return this.user.firstName && this.user.lastName && this.user.email;
    }

}