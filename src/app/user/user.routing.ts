import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user.component';
import { ProfileComponent } from './components/profile.component';
import { ChangePasswordComponent } from './components/change-password.component';
import { ChangeRoleComponent } from './components/change-role.component';

const routes: Routes = [
    {
        path: '',
        component: UserComponent,
        children: [
            {
                path: '',
                redirectTo: '/user/profile',
                pathMatch: 'full'
            },
            {
                path: 'profile',
                component: ProfileComponent,
                data: {
                    title: 'Persional Info'
                }
            },
            {
                path: 'change-password',
                component: ChangePasswordComponent,
                data: {
                    title: 'Change Password'
                }
            },
            {
                path: 'change-role',
                component: ChangeRoleComponent,
                data: {
                    title: 'Change Role'
                }
            },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class UserRouting { }
