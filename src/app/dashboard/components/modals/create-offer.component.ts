import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { RegexHelper } from '../../../shared/helpers/regex.helper';
import { VoluumService } from '../../../shared/services/voluum.service';

import * as _ from 'lodash';

@Component({
    selector: 'modal-offer',
    templateUrl: './templates/create-offer.component.html',
})

export class CreateOfferComponent implements OnInit {
    regexHelper = RegexHelper;

    title: string;
    button: string;
    onClose: Subject<boolean>;

    trackerId: string = '';

    @Input() offer: any = {
        namePostfix: "",
        url: "",
        country: { //optional
            code: "US"
        },
        affiliateNetwork: { //optional
            id: ""
        },
        payout: {
            type: "AUTO"
        },
        tags: [] //optional
    };

    urlTokens = [
        '{clickid}', '{campaign.id}', '{trafficsource.id}', '{trafficsource.name}', '{offer.id}', '{lander.id}', '{device}', '{brand}', '{model}', '{browser}', '{browserversion}', '{os}', '{osversion}', '{country}', '{countryname}',
        '{city}', '{region}', '{isp}', '{useragent}', '{ip}', '{var1}', '{var2}', '{var3}', '{var:variable name}', '{trackingdomain}', '{referrerdomain}', '{language}', '{connection.type}', '{carrier}'
    ];
    urlTokenSelected: Array<any> = [];
    affiliateNetworks: any;
    showCountrySelect: boolean = false;
    disableCountrySelect: boolean = true;
    showPayoutInput: boolean = false;

    constructor(
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private voluumService: VoluumService,
    ) {

    }

    ngOnInit() {
        this.onClose = new Subject();
    }

    showOfferModal(title: string, body: any, button: string, extras: any): void {
        this.title = title;
        this.offer = body ? body : this.offer;
        this.button = button;

        if (extras && extras.country)
            this.offer.country.code = extras.country;

        if (extras && extras.tracker_id)
            this.trackerId = extras.tracker_id;

        this.showCountrySelect = true;
        if (this.offer.country.code === 'null')
            this.disableCountrySelect = false;

        this.getAffiliateNetworks();
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        let obversable;
        if (this.offer && this.offer['id']) {
            const offer = {
                namePostfix: this.offer.namePostfix,
                url: this.offer.url,
                country: this.offer.country,
                affiliateNetwork: this.offer.affiliateNetwork,
                payout: this.offer.payout,
                tags: this.offer.tags
            };
            obversable = this.voluumService.updateOffer(this.offer['id'], offer, this.trackerId);
        } else {
            obversable = this.voluumService.createOffer(this.offer, this.trackerId);
        }

        obversable.subscribe(response => {
            this.offer = this.offer['id'] ? this.offer : response;
            this.messageService.flashSuccess("Successfully.");
            this.onClose.next(true);
            this.close();
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        if (!this.offer.namePostfix || !this.offer.url)
            return false;

        if (this.offer.payout.type === 'MANUAL') {
            if (this.offer.payout.value < 0.01 || this.offer.payout.value > 1000000)
                return this.messageService.flashWarning("Set payout value from 0.01 to 1,000,000.");
        }

        if (this.offer.affiliateNetwork.id === '') {
            delete this.offer['affiliateNetwork'];
        }

        return true;
    }

    getAffiliateNetworks() {
        this.voluumService.getAffiliateNetworks(this.trackerId).subscribe(result => {
            this.affiliateNetworks = result.affiliateNetworks;
        }, err => {
            this.messageService.flashError(err);
        });
    }

    onChangePayout() {
        if (this.offer.payout.type === 'AUTO') {
            this.offer.payout.value = 0;
            this.showPayoutInput = false;
        }
        else {
            this.offer.payout.value = "";
            this.showPayoutInput = true;
        }
    }

    onChangeUrl() {
        this.urlTokenSelected.forEach((elem, i) => {
            if (!_.includes(this.offer.url, elem))
                this.urlTokenSelected.splice(i, 1);
        });
    }

    addUrlToken(URLtoken) {
        if (!_.includes(this.urlTokenSelected, URLtoken)) {
            this.offer.url = `${this.offer.url}${URLtoken}`;
            this.urlTokenSelected.push(URLtoken);
        }
    }

    urlSelected(URLtoken) {
        if (_.includes(this.urlTokenSelected, URLtoken))
            return true;
        return false;
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }

}
