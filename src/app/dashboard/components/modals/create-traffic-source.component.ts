import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { TokenService } from '../../../shared/services/token.service';
import { UserService } from '../../../shared/services/user.service';
import { AccountsService } from '../../../shared/services/accounts.service';
import { TrackerService } from '../../../shared/services/tracker.service';
import { TrafficSourceService } from '../../../shared/services/traffic-source.service';
import { RegexHelper } from '../../../shared/helpers/regex.helper';

import * as _ from 'lodash';

@Component({
    selector: 'bs-modal-traffic-source',
    templateUrl: './templates/create-traffic-source.component.html',
})

export class CreateTrafficSourceComponent implements OnInit {
    regexHelper = RegexHelper;

    title: string;
    button: string;
    onClose: Subject<boolean>;

    @Input() trafficSource = {
        status: true,
        name: "",
        type: "Taboola",
        account_id: "",
        client_id: "",
        client_secret: "",
        username: "",
        email: "",
        password: "",
        trackers: [],
        user_id: '',
        created_at: new Date(),
    };

    trackerOptions: IMultiSelectOption[];
    trackerOptionSettings: IMultiSelectSettings = {
        enableSearch: false,
        checkedStyle: 'glyphicon',
        showCheckAll: false,
        showUncheckAll: false,
        dynamicTitleMaxItems: 3,
    };
    trackerOptionTexts: IMultiSelectTexts = {
        defaultTitle: 'Select tracker...',
    };

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private tokenService: TokenService,
        private accountsService: AccountsService,
        private trafficSourceService: TrafficSourceService,
        private trackerService: TrackerService,
    ) { }

    ngOnInit() {
        this.onClose = new Subject();
        this.loadTrackers();
    }

    showTrafficSourceModal(title: string, body: any, button: string): void {
        this.title = title;
        this.trafficSource = body ? body : this.trafficSource;
        this.button = button;
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        const userId = this.tokenService.getUserId();
        this.trafficSource.user_id = userId;

        const obversable = this.trafficSource && this.trafficSource['_id'] ?
            this.trafficSourceService.update(this.trafficSource['_id'], this.trafficSource) :
            this.trafficSourceService.create(this.trafficSource);

        obversable.subscribe(response => {
            this.trafficSource = this.trafficSource['_id'] ? this.trafficSource : response;
            this.messageService.flashSuccess(`${this.button} successfully.`);
            this.accountsService.remove();
            this.onClose.next(true);
            this.close();
        }, err => {
            this.messageService.flashError(err);
        });

    }

    canSubmit() {
        return this.trafficSource.name && this.trafficSource.type && this.trafficSource.trackers;
    }

    onChangeType() {

    }

    loadTrackers() {
        this.trackerService.list().subscribe(response => {
            this.trackerOptions = this.decodeTrackers(response);

            if (_.isEmpty(this.trackerOptions)) {
                this.messageService.flashWarning("Please add Tracker account.");
                return;
            }

            this.trackerOptions = _.map(this.trackerOptions, o => _.pick(o, ['_id', 'name']));
            let str = JSON.stringify(this.trackerOptions).replace(/_id/g, "id");
            this.trackerOptions = JSON.parse(str);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    decodeTrackers(data) {
        return JSON.parse(new Buffer(JSON.stringify(data), 'base64').toString());
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }
}
