import { BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Select2OptionData } from 'ng2-select2';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { CampaignService } from '../../../shared/services/campaigns.service';

import * as _ from 'lodash';

@Component({
    selector: 'modal-global-blacklist-site',
    templateUrl: './templates/blacklist-sites.component.html'
})

export class BlacklistModalComponent implements OnInit {
    title: string;
    onClose: Subject<boolean>;
    @Input() sites: Array<Select2OptionData>;
    totalSites: number;
    options: Select2Options;
    value: string[];
    current: string;

    constructor(
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private campaignService: CampaignService,
    ) { }

    ngOnInit() {
        this.onClose = new Subject();

        this.options = {
            width: '100%',
            multiple: true,
            theme: 'default',
            closeOnSelect: false
        }
    }

    showBlacklistModal(title: string, body: any): void {
        this.title = title;
        this.sites = body;
        this.totalSites = _.size(this.sites);

        const siteIds = _.map(this.sites, elem => elem.id);
        this.value = siteIds;
        this.current = this.value.join(' | ');
    }

    changed(data: { value: string[] }) {
        this.current = data.value.join(' | ');
        this.value = data.value;
    }

    onSubmit() {
        if (!this.canSubmit()) {
            return;
        }

        console.log(this.value);
    }

    canSubmit() {
        return this.value;
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }
}