import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs/Subject';
import { Observable, Subscription } from 'rxjs/Rx';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Select2OptionData } from 'ng2-select2';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CreateLanderComponent } from '../modals/create-lander.component';
import { CreateOfferComponent } from '../modals/create-offer.component';
import { CampaignService } from "../../../shared/services/campaigns.service";
import { WidgetService } from "../../../shared/services/widgets.service";
import { VoluumService } from '../../../shared/services/voluum.service';
import { TrackerService } from '../../../shared/services/tracker.service';
import { TrafficSourceService } from '../../../shared/services/traffic-source.service';

import * as _ from 'lodash';

@Component({
    selector: 'modal-campaign',
    templateUrl: './templates/create-campaign.component.html',
})

export class CreateCampaignComponent implements OnInit, OnDestroy {
    subscription: Subscription;

    title: string;
    button: string;
    onClose: Subject<boolean>;
    showLoading: boolean = false;

    callbackCountry: string;
    campaignDetail: any;
    trafficSources: Array<any> = [];
    trafficSourcesAccounts: any;
    trackerAccounts: any;
    landers: any;
    offers: any;
    prefixCampaignName: string;
    isDuplicate: boolean = false;
    oldCampName: string;

    switchButton = {
        onText: "On",
        offText: "Off",
        onColor: "green",
        offColor: "default",
        disabled: false,
        size: "normal",
    };

    blockedSites: Array<Select2OptionData>;
    blockedSitesOptions: Select2Options;
    blockedSitesCurrent: string;

    @Input() campaign: any = {
        namePostfix: "",
        type: "",
        traffic: "",
        tracker: "",
        payout: 0,
        blockSites: [],
        trafficSource: {
            id: ""
        },
        country: { //optional
            code: "US"
        },
        costModel: {
            type: "NOT_TRACKED"
        },
        redirectTarget: {
            inlineFlow: {
                defaultOfferRedirectMode: "REGULAR"
            },
        },
    };

    countPath: number = 1;
    defaultPaths = [
        {
            name: "Path 1",
            active: true,
            weight: 100,
            landers: [
                {
                    lander: {
                        id: ""
                    },
                    weight: 100
                },
            ],
            offers: [
                {
                    offer: {
                        id: ""
                    },
                    weight: 100
                },
            ],
            offerRedirectMode: "REGULAR",
            realtimeRoutingApiState: "DISABLED"
        },
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalRef: BsModalRef,
        private modalService: BsModalService,
        private messageService: MessageFlashService,
        private campaignService: CampaignService,
        private trackerService: TrackerService,
        private trafficSourceService: TrafficSourceService,
        private voluumService: VoluumService,
        private widgetService: WidgetService,
    ) { }

    ngOnInit() {
        this.onClose = new Subject();
        this.callbackCountry = this.onChangeCountry.bind(this);
        this.blockedSitesOptions = {
            width: "100%",
            theme: 'default',
            multiple: true,
            closeOnSelect: false
        }

        const getTrackers = this.trackerService.list();
        const getTraffics = this.trafficSourceService.list();

        this.subscription = Observable.forkJoin(getTrackers, getTraffics).subscribe(
            results => {
                this.trackerAccounts = this.decodeValue(results[0]);
                this.trafficSourcesAccounts = _.filter(this.decodeValue(results[1]), ['type', this.campaign.type]);
            },
            err => {
                this.messageService.flashError(err);
            }
        );
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    showCampaignModal(title: string, body: any, button: string): void {
        this.title = title;
        this.button = button;

        if (body) {
            this.campaignDetail = body.campaignData;
            this.campaign = body.campaignTracker;

            this.campaign._id = this.campaignDetail._id;
            this.campaign.type = this.campaignDetail.type;
            this.campaign.traffic = this.campaignDetail.trafficSourceAccount;
            this.campaign.tracker = this.campaignDetail.trackerAccount;
            this.campaign.payout = this.campaignDetail.payout;
            this.campaign.blockSites = this.campaignDetail.blockSites;
            this.defaultPaths = this.campaign.redirectTarget.inlineFlow.defaultPaths;

            this.requestDataTracker();
            this.updatePrefixName();

            if (this.campaign.type !== '') {
                this.blockingSites();
            }

            if (body.duplicate) {
                this.isDuplicate = true;
                this.oldCampName = this.campaign.namePostfix;
            }
        }
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        this.campaign.redirectTarget.inlineFlow.name = `${this.campaign.namePostfix} - inline flow`;
        this.pushDefaultPathToCampaign();

        let obversable;
        this.showLoading = true;
        if (this.campaign && this.campaign['id'] && !this.isDuplicate) {
            obversable = this.campaignService.update(this.campaign);
        } else {
            obversable = this.campaignService.create(this.campaign);
        }
        obversable.subscribe(response => {
            this.messageService.flashSuccess("Success.");
            this.showLoading = false;
            this.onClose.next(true);
            this.close();
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        if (!this.campaign.namePostfix || !this.campaign.trafficSource.id || !this.campaign.traffic ||
            !this.campaign.tracker || !this.campaign.country.code || !this.campaign.costModel.type) {
            return this.messageService.flashWarning("Please enter required field.");
        }

        if (!this.defaultPaths[0].offers[0].offer.id) {
            return this.messageService.flashWarning("Select at least 1 offer.");
        }

        if (this.isDuplicate && this.campaign.namePostfix === this.oldCampName) {
            return this.messageService.flashWarning("Name must be unique.");
        }

        return true;
    }

    decodeValue(data) {
        return JSON.parse(new Buffer(JSON.stringify(data), 'base64').toString());
    }

    pushDefaultPathToCampaign() {
        _.forEach(this.defaultPaths, path => {
            path.offerRedirectMode = this.campaign.redirectTarget.inlineFlow.defaultOfferRedirectMode;
        });
        this.campaign.redirectTarget.inlineFlow.defaultPaths = this.defaultPaths;
    }

    changeTrackerAccount() {
        this.campaign.trafficSource.id = '';
        this.requestDataTracker();
    }

    requestDataTracker() {
        if (!this.campaign.tracker)
            return;

        const getTrafficSources = this.voluumService.getTrafficSources(this.campaign.tracker);
        const getLanders = this.voluumService.getLanders(this.campaign.tracker);
        const getOffers = this.voluumService.getOffers(this.campaign.tracker);

        this.subscription = Observable.forkJoin(getTrafficSources, getLanders, getOffers).subscribe(
            results => {
                this.trafficSources = results[0].trafficSources;
                this.landers = _.filter(results[1].landers, ['country.code', this.campaign.country.code]);
                this.offers = _.filter(results[2].offers, ['country.code', this.campaign.country.code]);;
            },
            err => {
                this.messageService.flashError(err);
            }
        );
    }

    changeType() {
        if (!this.campaign.trafficSource.id)
            return;

        this.campaign.traffic = '';
        this.campaign.type = _.find(this.trafficSources, ['id', this.campaign.trafficSource.id]).name;
        this.updatePrefixName();
        this.blockingSites();
        this.trafficSourceService.list().subscribe(results => {
            this.trafficSourcesAccounts = _.filter(this.decodeValue(results), ['type', this.campaign.type]);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    onChangeCountry(countryCode) {
        if (!this.campaign.tracker)
            return;

        this.updatePrefixName();

        const getLanders = this.voluumService.getLanders(this.campaign.tracker);
        const getOffers = this.voluumService.getOffers(this.campaign.tracker);

        this.subscription = Observable.forkJoin(getLanders, getOffers).subscribe(
            results => {
                this.landers = _.filter(results[0].landers, ['country.code', countryCode]);
                this.offers = _.filter(results[1].offers, ['country.code', countryCode]);;
            },
            err => {
                this.messageService.flashError(err);
            }
        );
    }

    updatePrefixName() {
        const type = this.campaign.type;
        const country = this.campaign.country.code;
        const name = _.join([type, country], " - ");
        this.prefixCampaignName = `${name} -`;
    }

    blockingSites() {
        this.widgetService.getBlockedSites(this.campaign.type).subscribe(results => {
            const blocked_sites = _.map(results, e => {
                return { id: e.site_id, text: e.site_name };
            });
            this.blockedSites = _.sortBy(blocked_sites, o => o.text);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    changedBlockedSites(data: { value: string[] }) {
        if (data.value) {
            this.campaign.blockSites = data.value;
        } else {
            this.campaign.blockSites = [];
        }
    }

    onFlagChange(status: boolean, path) {
        path.active = status;
    }

    removePath(pathIndex) {
        if (Object.keys(this.defaultPaths).length == 1) {
            return this.messageService.flashWarning("Must have at least 1 path.");
        }

        this.defaultPaths.splice(pathIndex, 1);
    }

    addPath() {
        this.countPath = Object.keys(this.defaultPaths).length;
        this.defaultPaths.push({
            name: `Path ${this.countPath + 1}`,
            active: true,
            weight: 100,
            landers: [
                {
                    lander: {
                        id: ""
                    },
                    weight: 100
                },
            ],
            offers: [
                {
                    offer: {
                        id: ""
                    },
                    weight: 100
                },
            ],
            offerRedirectMode: "REGULAR",
            realtimeRoutingApiState: "DISABLED"
        });
    }

    removeLander(landerIndex, path) {
        path.landers.splice(landerIndex, 1);
    }

    editLander(landers) {
        const landerId = landers.lander.id;

        if (!landerId)
            return this.messageService.flashWarning("Select a lander.");


        this.subscription = this.voluumService.getLander(landerId, this.campaign.tracker).subscribe(lander => {
            const title = `Edit Lander: ${lander.name}`;
            const button = "Save Changes";
            const extras = {
                tracker_id: this.campaign.tracker,
            };
            this.showLanderPopup(title, lander, button, extras);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    showLanderPopup(title, lander, button, extras = null) {
        const modal = this.modalService.show(CreateLanderComponent);
        (<CreateLanderComponent>modal.content).showLanderModal(title, lander, button, extras);
        (<CreateLanderComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.requestLanderList();
            }
        });
    }

    requestLanderList() {
        this.subscription = this.voluumService.getLanders(this.campaign.tracker).subscribe(result => {
            this.landers = result.landers;
        }, err => {
            this.messageService.flashError(err);
        });
    }

    addLander(path) {
        path.landers.push({
            lander: {
                id: ""
            },
            weight: 100
        });
    }

    newLander() {
        const title = "New Lander";
        const button = "Create";
        const extras = {
            country: this.campaign.country.code,
            tracker_id: this.campaign.tracker,
        };
        this.showLanderPopup(title, null, button, extras);
    }

    removeOffer(offerIndex, path) {
        if (Object.keys(path.offers).length == 1) {
            return this.messageService.flashWarning("Must have at least 1 offer.");
        }
        path.offers.splice(offerIndex, 1);
    }

    editOffer(offers) {
        const offerId = offers.offer.id;

        if (!offerId)
            return this.messageService.flashWarning("Select a offer.");

        this.subscription = this.voluumService.getOffer(offerId, this.campaign.tracker).subscribe(offer => {
            const title = `Edit Offer: ${offer.name}`;
            const button = "Save Changes";
            const extras = {
                tracker_id: this.campaign.tracker,
            };
            this.showOfferPopup(title, offer, button, extras);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    showOfferPopup(title, offer, button, extras = null) {
        const modal = this.modalService.show(CreateOfferComponent);
        (<CreateOfferComponent>modal.content).showOfferModal(title, offer, button, extras);
        (<CreateOfferComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.requestOfferList();
            }
        });
    }

    requestOfferList() {
        this.subscription = this.voluumService.getOffers(this.campaign.tracker).subscribe(result => {
            this.offers = result.offers;
        }, err => {
            this.messageService.flashError(err);
        });
    }

    addOffer(path) {
        path.offers.push({
            offer: {
                id: ""
            },
            weight: 100
        });
    }

    newOffer() {
        const title = "New Offer";
        const button = "Create";
        const extras = {
            country: this.campaign.country.code,
            tracker_id: this.campaign.tracker,
        };
        this.showOfferPopup(title, null, button, extras);
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
