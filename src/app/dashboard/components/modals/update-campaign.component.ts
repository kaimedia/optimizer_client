import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { CampaignService } from "../../../shared/services/campaigns.service";

import * as _ from 'lodash';

@Component({
    selector: 'modal-update-bid',
    templateUrl: './templates/update-campaign.component.html',
})

export class UpdateCampaignComponent implements OnInit {
    title: string;
    button: string;
    onClose: Subject<boolean>;
    showLoading: boolean = false;

    @Input() campaign = {
        id: "",
        name: "",
        cpc: 0,
        spending_limit: 0,
        daily_ad_delivery_model: "",
    };

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private campaignService: CampaignService,
    ) { }

    ngOnInit() {
        this.onClose = new Subject();
    }

    showCampaignModal(title: string, body: any, button: string): void {
        this.title = title;
        this.campaign = body ? body : this.campaign;
        this.button = button;
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        this.showLoading = true;
        this.campaignService.changeBid(this.campaign).subscribe(response => {
            this.messageService.flashSuccess('Successfully.');
            this.showLoading = false;
            this.onClose.next(true);
            this.close();
        }, err => {
            this.messageService.flashError(err);
            this.showLoading = false;
        });
    }

    canSubmit() {
        return this.campaign.id && this.campaign.cpc && this.campaign.spending_limit;
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }
}
