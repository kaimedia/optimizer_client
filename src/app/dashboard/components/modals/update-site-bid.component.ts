import { BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { CampaignService } from '../../../shared/services/campaigns.service';

import * as _ from 'lodash';

@Component({
    selector: 'modal-update-site-bid',
    templateUrl: './templates/update-site-bid.component.html'
})

export class SiteBidModalComponent implements OnInit {
    title: string;
    onClose: Subject<any>;
    campaign: any;
    alertFixedCPC: string;
    alertCPCPercent: string;
    showLoading: boolean = false;
    dateRange: any;

    @Input() site: any = {
        configured_cpc: 0,
        cpc_boost: '',
        cpc_boost_percent: 0,
        boosted_cpc: 0,
    };

    public constructor(
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private campaignService: CampaignService,
    ) { }

    public ngOnInit(): void {
        this.onClose = new Subject();
    }

    public showSiteBidModal(title: string, item: any, campaign: any, dateRange: any): void {
        this.title = title;
        this.site = item;
        this.campaign = campaign;
        this.dateRange = dateRange;
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        this.showLoading = true;
        this.campaignService.changeSiteCpc({ site: this.site }).subscribe(res => {
            this.messageService.flashSuccess(res.message);
            this.onClose.next({ status: true, data: this.site });
            this.close();
            this.showLoading = false;
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        return this.site.site_id;
    }

    onChangeFixedCpc() {
        this.validates();
        this.site.cpc_boost_percent = _.round(((this.site.boosted_cpc - this.site.configured_cpc) / this.site.configured_cpc) * 100);
        this.site.cpc_boost = _.round(this.site.cpc_boost_percent / 100, 2);
    }

    onChangeCpcPercent() {
        this.validates();
        this.site.cpc_boost = _.round(this.site.cpc_boost_percent / 100, 2);
        this.site.boosted_cpc = _.round(_.add(_.multiply(this.site.configured_cpc, this.site.cpc_boost), this.site.configured_cpc), 3);
    }

    validates() {
        this.hideAlert();

        if (!this.site.boosted_cpc && this.site.boosted_cpc !== 0)
            return this.alertFixedCPC = 'Fixed CPC cannot be empty';

        if (!this.site.cpc_boost_percent && this.site.cpc_boost_percent !== 0)
            return this.alertCPCPercent = 'CPC adjustment cannot be empty';

        if (this.site.boosted_cpc <= 0)
            return this.alertFixedCPC = 'Fixed CPC must be higher than zero';

        if (this.campaign.type === 'Outbrain') {
            if (this.site.cpc_boost_percent >= 1000)
                return this.alertCPCPercent = 'CPC adjustment must be lower than 1000.';

            if (this.site.boosted_cpc > 0 && this.site.boosted_cpc < this.campaign.minimumCpc)
                this.alertFixedCPC = `The minimum CPC of ${this.campaign.minimumCpc} will be used.`;
        }
    }

    reset() {
        this.site.cpc_boost = '';
        this.site.cpc_boost_percent = 0;
        this.site.boosted_cpc = this.site.configured_cpc;
    }

    hideAlert() {
        this.alertFixedCPC = "";
        this.alertCPCPercent = "";
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }

}