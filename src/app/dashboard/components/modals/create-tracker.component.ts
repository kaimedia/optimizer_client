import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { TokenService } from '../../../shared/services/token.service';
import { AccountsService } from '../../../shared/services/accounts.service';
import { TrackerService } from '../../../shared/services/tracker.service';

import * as _ from 'lodash';

@Component({
    selector: 'bs-modal-tracker',
    templateUrl: './templates/create-tracker.component.html',
})

export class CreateTrackerComponent implements OnInit {
    title: string;
    button: string;
    onClose: Subject<boolean>;

    @Input() tracker = {
        status: true,
        name: "",
        type: "Voluum",
        access_id: "",
        access_key: "",
        user_id: '',
        created_at: new Date(),
    };

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private tokenService: TokenService,
        private accountsService: AccountsService,
        private trackerService: TrackerService,
    ) { }

    ngOnInit() {
        this.onClose = new Subject();
    }

    showTrackerModal(title: string, body: any, button: string): void {
        this.title = title;
        this.tracker = body ? body : this.tracker;
        this.button = button;
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        const userId = this.tokenService.getUserId();
        this.tracker.user_id = userId;

        const obversable = this.tracker && this.tracker['_id'] ?
            this.trackerService.update(this.tracker['_id'], this.tracker) :
            this.trackerService.create(this.tracker);

        obversable.subscribe(response => {
            this.tracker = this.tracker['_id'] ? this.tracker : response;
            this.messageService.flashSuccess(`${this.button} successfully.`);
            this.accountsService.remove();
            this.onClose.next(true);
            this.close();
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        return this.tracker.name && this.tracker.type && this.tracker.access_id && this.tracker.access_key;
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }
}
