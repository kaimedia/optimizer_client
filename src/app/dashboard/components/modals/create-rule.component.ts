import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { RuleService } from '../../../shared/services/rule.service';
import { CampaignService } from "../../../shared/services/campaigns.service";

import * as _ from 'lodash';

@Component({
    selector: 'bs-modal-rule',
    templateUrl: './templates/create-rule.component.html',
})

export class CreateRuleComponent implements OnInit {
    title: string;
    button: string;
    onClose: Subject<boolean>;

    showBlockConditions: boolean = false;
    showDecreaseCpcConditions: boolean = false;

    @Input() rule: any = {
        status: true,
        name: "",
        action: 3,
        interval: '30days',
        rotation: '45min',
        level: 3,
        campaigns: [],
        criterias: [
            {
                column: 'net',
                operator: 'less',
                value: '0',
                unit: '$',
                calc: 'multiply',
                xcolumn: 'ap',
                onCalc: false,
            }
        ],
        decrease_cpc: [
            {
                column: 'cost',
                operator: 'greater',
                value: 10,
                unit: '$',
            },
            {
                column: 'cpm',
                operator: 'greater',
                value: 0.3,
                unit: '$',
            }
        ],
        created_at: new Date(),
    };

    campaignOptions: IMultiSelectOption[];
    campaignOptionSettings: IMultiSelectSettings = {
        enableSearch: true,
        checkedStyle: 'glyphicon',
        showCheckAll: false,
        showUncheckAll: false,
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: true,
    };
    campaignOptionTexts: IMultiSelectTexts = {
        defaultTitle: 'Select campaign...',
    };

    actionOptions = [
        { id: 1, name: 'Email Alert' },
        { id: 2, name: 'Block' },
        { id: 3, name: 'Block & Blacklist' },
        { id: 4, name: 'Increase CPC', disabled: true },
        { id: 5, name: 'Decrease CPC' },
    ];

    intervalOptions = [
        { id: 'today', name: 'Today' },
        { id: 'yesterday', name: 'Yesterday' },
        { id: '3days', name: 'Last 3 days' },
        { id: '7days', name: 'Last 7 days' },
        { id: '14days', name: 'Last 14 days' },
        { id: '30days', name: 'Last 30 days' },
        { id: 'lifetime', name: 'Lifetime' },
    ];

    rotationOptions = [
        { id: '10min', name: 'Every 10 Mins' },
        { id: '15min', name: 'Every 15 Mins' },
        { id: '20min', name: 'Every 20 Mins' },
        { id: '30min', name: 'Every 30 Mins' },
        { id: '45min', name: 'Every 45 Mins' },
        { id: '1hour', name: 'Every 1 Hour' },
        { id: '2hour', name: 'Every 2 Hours' },
        { id: '3hour', name: 'Every 3 Hours' },
        { id: '6hour', name: 'Every 6 Hours' },
        { id: '12hour', name: 'Every 12 Hours' },
        { id: 'daily', name: 'Daily' },
        { id: 'weekly', name: 'Weekly' },
    ];

    levelOptions = [
        { id: 1, name: 'Only on Campaigns', disabled: true },
        { id: 2, name: 'Only on Contents', disabled: true },
        { id: 3, name: 'Only on Sites' },
    ];

    conditionColumns = [
        { id: 'bot', name: 'Percent Bot' },
        { id: 'block_bot', name: 'Bot Clicks' },
        { id: 'impressions', name: 'Impressions' },
        { id: 'clicks', name: 'TS Clicks' },
        { id: 'lp_clicks', name: 'LP Clicks' },
        { id: 'trk_clicks', name: 'Trk Clicks' },
        { id: 'ctr', name: 'TS CTR' },
        { id: 'lp_ctr', name: 'LP CTR' },
        { id: 'conversions', name: 'Conversions' },
        { id: 'cost', name: 'Cost' },
        { id: 'revenue', name: 'Revenue' },
        { id: 'cpc', name: 'CPC' },
        { id: 'epc', name: 'EPC' },
        { id: 'epv', name: 'EPV' },
        { id: 'cpm', name: 'CPM' },
        { id: 'cpa', name: 'CPA' },
        { id: 'cpv', name: 'CPV' },
        { id: 'net', name: 'NET' },
        { id: 'roi', name: 'ROI' },
        { id: 'ap', name: 'AP' }
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private ruleService: RuleService,
        private campaignService: CampaignService,
    ) { }

    ngOnInit() {
        this.onClose = new Subject();
        this.activatedRoute.data.subscribe(data => {
            this.loadCampaigns();
        });
    }

    showRuleModal(title: string, body: any, button: string, extras: any = null): void {
        this.title = title;
        this.rule = body ? body : this.rule;
        this.button = button;

        if (extras && extras.campaign_id) {
            this.rule.campaigns.push(extras.campaign_id);
        }
        this.clearElements();
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        const obversable = this.rule && this.rule['_id'] ?
            this.ruleService.update(this.rule['_id'], this.rule) :
            this.ruleService.create(this.rule);

        obversable.subscribe(response => {
            this.rule = this.rule['_id'] ? this.rule : response;
            this.messageService.flashSuccess(`${this.button} successfully.`);
            this.onClose.next(true);
            this.close();
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        if (!this.rule.name || !this.rule.campaigns || !this.rule.criterias)
            return this.messageService.flashWarning("Please enter all fields is requrired.");

        return true;
    }

    onChangeAction() {
        this.clearElements();
    }

    clearElements() {
        if (this.rule.action == 4 || this.rule.action == 5) {
            this.showBlockConditions = false;
            this.showDecreaseCpcConditions = true;
            // _.remove(this.rotationOptions, o => _.includes(['10min', '15min'], o.id));
        }
        else {
            this.showBlockConditions = true;
            this.showDecreaseCpcConditions = false;
        }
    }

    onChangeCampaigns() {
        if (_.includes(this.rule.campaigns, 'global'))
            this.rule.campaigns = ["global"];

        if (_.includes(this.rule.campaigns, 'all'))
            this.rule.campaigns = ["all"];
    }

    loadCampaigns() {
        this.campaignService.getAllCampaigns().subscribe(response => {
            this.campaignOptions = _.reverse(_.map(response, o => {
                return { id: o._id, name: o.name };
            }));
            this.campaignOptions.unshift(
                { id: 'all', name: 'All campaigns' }
            );
        }, err => {
            this.messageService.flashError(err);
        });
    }

    compare(operator, item) {
        item.operator = operator;
    }

    addCriteria() {
        this.rule.criterias.push({
            column: 'net',
            operator: 'less',
            value: '0',
            unit: '$',
            calc: 'multiply',
            xcolumn: 'ap',
            onCalc: false,
        });
    }

    deleteCriteria(rowIndex) {
        this.rule.criterias.splice(rowIndex, 1);
    }

    changeRuleColumn(item) {
        switch (item.column) {
            case 'net':
            case 'revenue':
            case 'cost':
            case 'cpc':
            case 'epc':
            case 'cpa':
            case 'epv':
            case 'cpv':
            case 'cpm':
            case 'ap':
                item.unit = '$';
                break;

            case 'bot':
            case 'ts_ctr':
            case 'lp_ctr':
            case 'roi':
            case 'cv':
                item.unit = '%';
                break;

            default:
                item.unit = '';
                break;
        }
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }
}
