import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { RegexHelper } from '../../../shared/helpers/regex.helper';
import { VoluumService } from '../../../shared/services/voluum.service';

import * as _ from 'lodash';

@Component({
    selector: 'modal-lander',
    templateUrl: './templates/create-lander.component.html',
})

export class CreateLanderComponent implements OnInit {
    regexHelper = RegexHelper;

    title: string;
    button: string;
    onClose: Subject<boolean>;

    trackerId: string = '';

    @Input() lander = {
        namePostfix: "",
        url: "",
        numberOfOffers: 1,
        country: { //optional
            code: "US"
        },
        tags: [], //optional
    };

    urlTokens = [
        '{campaign.id}', '{trafficsource.id}', '{trafficsource.name}', '{offer.id}', '{lander.id}', '{device}', '{brand}', '{model}', '{browser}', '{browserversion}', '{os}', '{osversion}', '{country}', '{countryname}',
        '{city}', '{region}', '{isp}', '{useragent}', '{ip}', '{var1}', '{var2}', '{var3}', '{var:variable name}', '{trackingdomain}', '{referrerdomain}', '{language}', '{connection.type}', '{carrier}'
    ];
    urlTokenSelected: Array<any> = [];
    showCountrySelect: boolean = false;
    disableCountrySelect: boolean = true;

    constructor(
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private voluumService: VoluumService,
    ) {

    }

    ngOnInit() {
        this.onClose = new Subject();
    }

    showLanderModal(title: string, body: any, button: string, extras: any): void {
        this.title = title;
        this.lander = body ? body : this.lander;
        this.button = button;

        if (extras && extras.country)
            this.lander.country.code = extras.country;

        if (extras && extras.tracker_id)
            this.trackerId = extras.tracker_id;

        this.showCountrySelect = true;
        if (this.lander.country.code === 'null')
            this.disableCountrySelect = false;
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        let obversable;
        if (this.lander && this.lander['id']) {
            const lander = {
                namePostfix: this.lander.namePostfix,
                url: this.lander.url,
                numberOfOffers: this.lander.numberOfOffers,
                country: this.lander.country,
                tags: this.lander.tags
            };
            obversable = this.voluumService.updateLander(this.lander['id'], lander, this.trackerId);
        } else {
            obversable = this.voluumService.createLander(this.lander, this.trackerId);
        }

        obversable.subscribe(response => {
            this.lander = this.lander['id'] ? this.lander : response;
            this.messageService.flashSuccess("Successfully.");
            this.onClose.next(true);
            this.close();
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        if (!this.lander.namePostfix || !this.lander.url)
            return false;

        if (this.lander.numberOfOffers < 1 || this.lander.numberOfOffers > 20) {
            this.messageService.flashWarning("Select number of offers from 1 to 20.");
            return false;
        }

        return true;
    }

    onChangeUrl() {
        this.urlTokenSelected.forEach((elem, i) => {
            if (!_.includes(this.lander.url, elem))
                this.urlTokenSelected.splice(i, 1);
        });
    }

    addUrlToken(URLtoken) {
        if (!_.includes(this.urlTokenSelected, URLtoken)) {
            this.lander.url = `${this.lander.url}${URLtoken}`;
            this.urlTokenSelected.push(URLtoken);
        }
    }

    urlSelected(URLtoken) {
        if (_.includes(this.urlTokenSelected, URLtoken))
            return true;
        return false;
    }

    close(): void {
        this.onClose.next(false);
        this.modalRef.hide();
    }
}
