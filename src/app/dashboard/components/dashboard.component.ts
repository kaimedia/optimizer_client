import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subscription } from 'rxjs/Rx';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { ConfirmationModalComponent } from './modals/confirm.component';
import { UpdateCampaignComponent } from './modals/update-campaign.component';
import { environment } from '../../../environments/environment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { StorageService } from '../../core/services/storage.service';
import { AccountsService } from '../../shared/services/accounts.service';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { RoleService } from '../../shared/services/role.service';
import { DateRangeService } from "../../shared/services/date-range.service";
import { CampaignService } from '../../shared/services/campaigns.service';
import { FraudService } from '../../shared/services/fraud.service';
import { TrafficSourceService } from '../../shared/services/traffic-source.service';
import { TrackerService } from '../../shared/services/tracker.service';

import * as moment from "moment-timezone";
import * as _ from 'lodash';

const dateFormat = 'YYYY-MM-DD';
const timerRefresh = environment.refreshTime;
moment.tz.setDefault(environment.timeZone);

@Component({
    selector: 'app-dashboard',
    templateUrl: './templates/dashboard.component.html',
})

export class DashboardComponent implements OnInit, OnDestroy {
    @ViewChild("baseChart") chart: BaseChartDirective;

    title: string = "Dashboard";
    titleCampaign: string;
    private subscription: Subscription;
    private timerSubscription;
    systemTime: string;
    showLoading: boolean = false;
    campaignAll: any;
    campaignList: any;
    campaignCount: number = 0;
    statTotals: any;
    filterDate: Object;
    filterQuery: string;
    filterStatus: string = "";
    selectedAll: any;

    tableOptions = {
        rowsOnPage: 25,
        sortBy: "cost",
        sortOrder: "desc",
        columnCount: 50
    };

    tableColumnModel: any = [
        'id', 'name', 'status', 'impressions', 'ctr', 'conversions', 'cost', 'revenue', 'net', 'roi',
        'clicks', 'trk_clicks', 'lp_clicks', 'lp_ctr', 'rmk_clicks', 'rmk_conversions', 'rmk_cost'];

    tableColumns: IMultiSelectOption[] = [
        { id: 'id', name: 'Camp.ID' },
        { id: 'name', name: 'Name' },
        { id: 'status', name: 'Status' },
        { id: 'impressions', name: 'Impressions' },
        { id: 'clicks', name: 'Clicks' },
        { id: 'trk_clicks', name: 'Trk Clicks' },
        { id: 'lp_clicks', name: 'LP Clicks' },
        { id: 'ctr', name: 'CTR' },
        { id: 'lp_ctr', name: 'LP Ctr' },
        { id: 'conversions', name: 'Conversions' },
        { id: 'cost', name: 'Cost' },
        { id: 'revenue', name: 'Revenue' },
        { id: 'cpm', name: 'CPM' },
        { id: 'net', name: 'Net' },
        { id: 'roi', name: 'ROI' },
        { id: 'cv', name: 'CV' },
        { id: 'epc', name: 'EPC' },
        { id: 'epv', name: 'EPV' },
        { id: 'cpv', name: 'CPV' },
        { id: 'cpa', name: 'CPA' },
        { id: 'ap', name: 'AP' },
        { id: 'rmk_clicks', name: 'Remarketing Clicks' },
        { id: 'rmk_cost', name: 'Remarketing Cost' },
        { id: 'rmk_conversions', name: 'Remarketing Conv' },
    ];

    tableColumnSettings: IMultiSelectSettings = {
        fixedTitle: true,
        checkedStyle: 'glyphicon',
        buttonClasses: 'btn btn-primary',
        displayAllSelectedText: true,
        pullRight: true,
        showCheckAll: true,
        showUncheckAll: true,
        maxHeight: '400px',
    };

    tableColumnTexts: IMultiSelectTexts = {
        defaultTitle: 'Columns',
    };

    dateRange: any = {
        start_date: moment().format(dateFormat),
        end_date: moment().format(dateFormat)
    };

    TrafficSourceList: Array<any> = [];
    TrackerList: Array<any> = [];

    accountAPI = {
        traffic_source: {
            type: 'Taboola',
            id: '',
            name: '',
        },
        tracker: {
            type: 'Voluum',
            id: '',
            name: '',
        },
    };

    TrafficSourceTypes: Array<any> = [
        { id: "Taboola", name: "Taboola" },
        { id: "Outbrain", name: "Outbrain" },
    ];

    lineChartData: Array<any>;
    lineChartLabels: Array<any>;
    lineChartOptions: any = {
        responsive: true
    };
    lineChartColors: Array<any> = [
        {
            borderColor: 'rgb(255, 99, 132)',
            backgroundColor: 'rgba(255, 99, 132, 0.1)',
        },
        {
            borderColor: 'rgb(54, 162, 235)',
            backgroundColor: 'rgba(54, 162, 235, 0.1)',
        },
    ];
    lineChartLegend: boolean = true;
    lineChartType: string = 'line';

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private storageService: StorageService,
        private accountsService: AccountsService,
        private modalService: BsModalService,
        private messageFlashService: MessageFlashService,
        private roleService: RoleService,
        private dateRangeService: DateRangeService,
        private campaignService: CampaignService,
        private fraudService: FraudService,
        private trafficSourceService: TrafficSourceService,
        private trackerService: TrackerService,
    ) { }

    ngOnInit() {
        this.tableColumnModel = JSON.parse(this.storageService.get('campaign_columns')) || this.tableColumnModel;
        this.dateRange = this.dateRangeService.get() || this.dateRange;
        this.filterDate = this.dateFilterResult.bind(this);
        this.activatedRoute.parent.parent.data.subscribe((data: { user }) => {
            this.roleService.addRole(data.user.role);
            this.connectingAccounts();
            this.subscribeTimerInit();
        });
    }

    ngOnDestroy() {
        this.subscriptionInit();

        if (this.timerSubscription) {
            this.timerSubscription.unsubscribe();
        }
    }

    dateFilterResult(date: Object) {
        this.dateRange = date;
        this.multipleData(this.dateRange, 1);
    }

    connectingAccounts() {
        this.start();
        this.accountDefaults();
        this.campaignList = [];

        const accountStored = this.accountsService.get();
        const getTrafficSources = this.trafficSourceService.list();
        const getTrackers = this.trackerService.list();

        this.subscriptionInit();
        this.subscription = Observable.forkJoin(getTrafficSources, getTrackers).subscribe(result => {
            this.filterTsAccounts(result[0]);
            if (_.isEmpty(this.TrafficSourceList))
                return this.messageFlashService.flashWarning("Traffic Source account is not exists.");

            this.accountAPI.traffic_source.id = accountStored ? accountStored.traffic_source.id : this.TrafficSourceList[0]._id;
            this.accountAPI.traffic_source.name = accountStored ? accountStored.traffic_source.name : this.TrafficSourceList[0].name;
            this.accountAPI.traffic_source.type = accountStored ? accountStored.traffic_source.type : this.TrafficSourceList[0].type;

            this.filterTrkAccounts(result[1], this.TrafficSourceList, this.accountAPI.traffic_source.id);
            if (_.isEmpty(this.TrackerList))
                return this.messageFlashService.flashWarning("Tracker Account is not exists.");

            this.accountAPI.tracker.id = accountStored ? accountStored.tracker.id : this.TrackerList[0]._id;
            this.accountAPI.tracker.name = accountStored ? accountStored.tracker.name : this.TrackerList[0].name;
            this.accountAPI.tracker.type = accountStored ? accountStored.tracker.type : this.TrackerList[0].type;

            this.requestData();

        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    requestData() {
        this.setLinkedAccounts();
        this.multipleData(this.dateRange);
    }

    refreshSystemTime() {
        this.systemTime = moment().format('dddd, DD MMMM YYYY, HH:mm A Z');
    }

    multipleData(filter: any, cache: number = 1) {
        this.start();
        this.refreshSystemTime();

        const campaignList = this.campaignService.campaignMetrics(this.accountAPI.traffic_source.type, filter);
        const statsFraud = this.fraudService.statsAll(filter);

        this.subscriptionInit();
        this.subscription = Observable.forkJoin(campaignList, statsFraud).subscribe(
            results => {
                this.campaignList = results[0].campaigns;
                this.campaignCount = results[0].totalRows;
                this.statTotals = results[0].summary;
                this.statTotals.remarketing = true;
                this.handlerFraudData(results[1].data);
                this.setTitleCampaign();
                this.selectAll();
                this.complete();
            },
            err => {
                this.messageFlashService.flashWarning(err);
                this.complete();
            }
        );
    }

    getCampaignList(filter: any, cache: number = 1) {
        this.start();
        this.subscriptionInit();
        this.subscription = this.campaignService.campaignMetrics(this.accountAPI.traffic_source.type, filter).subscribe(results => {
            this.campaignList = results.campaigns;
            this.campaignCount = results.totalRows;
            this.statTotals = results.summary;
            this.statTotals.remarketing = true;
            this.complete();
        }, err => {
            this.messageFlashService.flashError(err);
            this.complete();
        });
    }

    accountDefaults() {
        const accountStored = this.accountsService.get();
        this.TrafficSourceList = this.TrackerList = [];
        this.accountAPI.traffic_source.id = this.accountAPI.tracker.id = "";
        this.accountAPI.traffic_source.type = accountStored ? accountStored.traffic_source.type : this.accountAPI.traffic_source.type;
    }

    setLinkedAccounts() {
        this.accountsService.set(this.accountAPI);
    }

    setTitleCampaign() {
        this.titleCampaign = `${this.accountAPI.traffic_source.type} Campaigns`;
    }

    filterTsAccounts(sources) {
        sources = this.decodeAccounts(sources);
        this.TrafficSourceList = _.filter(sources, o => o.type === this.accountAPI.traffic_source.type && o.status);
    }

    filterTrkAccounts(trackers, sources, sourceId = null) {
        trackers = this.decodeAccounts(trackers);
        if (sourceId)
            sources = _.filter(sources, o => sourceId === o._id);

        const trackerIDs = _.isArray(sources) ? sources[0].trackers : sources.trackers;
        this.TrackerList = _.filter(trackers, o => _.includes(trackerIDs, o._id) && o.status);
    }

    decodeAccounts(data) {
        return JSON.parse(new Buffer(JSON.stringify(data), 'base64').toString());
    }

    changeTrafficSourceType() {
        this.accountsService.remove();
        this.connectingAccounts();
    }

    changeTrafficSourceAccount() {
        this.trafficSourceService.get(this.accountAPI.traffic_source.id).subscribe(source => {
            source = this.decodeAccounts(source);
            this.accountAPI.traffic_source.name = source.name;
            this.trackerService.list().subscribe(trackers => {
                this.filterTrkAccounts(trackers, source);
                if (!_.isEmpty(this.TrackerList)) {
                    this.accountAPI.tracker.id = this.TrackerList[0]._id;
                    this.accountAPI.tracker.name = this.TrackerList[0].name;
                    this.accountAPI.tracker.type = this.TrackerList[0].type;
                    this.requestData();
                } else {
                    this.messageFlashService.flashWarning("Tracker Account is not exists.");
                }
            }, err => {
                this.messageFlashService.flashError(err);
            });
        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    changeTrackerAccount() {
        this.subscriptionInit();
        this.subscription = this.trackerService.get(this.accountAPI.tracker.id).subscribe(tracker => {
            tracker = this.decodeAccounts(tracker);
            this.accountAPI.tracker.name = tracker.name;
            this.requestData();
        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    subscribeTimerInit(): void {
        this.timerSubscription = Observable.interval(timerRefresh).subscribe(() => this.multipleData(this.dateRange, 0));
    }

    subscribeTimerToData(): void {
        this.timerSubscription = Observable.timer(timerRefresh).first().subscribe(() => this.multipleData(this.dateRange, 0));
    }

    handlerFraudData(stats) {
        if (!_.isEmpty(stats)) {
            let totals = _.map(stats.daily, o => o.total);
            let blocked = _.map(stats.daily, o => o.secondary);
            this.lineChartData = [
                { data: blocked, label: 'Blocked' },
                { data: totals, label: 'Total' }
            ];
            this.lineChartLabels = _.map(stats.daily, o => o.date);

            this.reloadChart(this.lineChartData, this.lineChartLabels);
        }
    }

    reloadCampaign(): void {
        this.getCampaignList(this.dateRange);
    }

    reloadChart(datasets, labels) {
        if (this.chart !== undefined) {
            if (!_.isUndefined(this.chart.datasets))
                this.chart.datasets = datasets;

            this.chart.labels = labels;
            this.chart.ngOnInit();
        }
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    removeItem(item: any) {
        this.campaignList = _.filter(this.campaignList, (elem) => elem != item);
    }

    calcClicks(x: number, y: number): any {
        return (x - y);
    }

    clearSearchText(): void {
        this.filterQuery = "";
    }

    showItemId(item: any) {
        return item.id;

        // if (_.size(item.id) <= 10)
        //     return item.id;
        // else
        //     return `${item.id.slice(0, 10)}******`;
    }

    showCalcClicks(x: number, y: number): boolean {
        let subc = this.calcClicks(x, y);
        if ((subc != 0) && !isNaN(subc))
            return true;
        else
            return false;
    }

    renderCalcClicks(x: number, y: number): string {
        let subc = this.calcClicks(x, y);
        if (subc > 0)
            return ('+' + subc);
        else
            return subc;
    }

    campaignStatus(item: any, status: boolean) {
        let title: string = "";
        let body: string = "";

        if (status === false) {
            title = "Confirm Stop Campaign";
            body = `Are you sure to stop campaign: ${item.name}?`;
        } else if (status === true) {
            title = "Confirm Resume Campaign";
            body = `Are you sure to resume campaign: ${item.name}?`;
        }

        this.updateConfirmModal(item, title, body, status);
    }

    updateConfirmModal(item, title, text, status) {
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, text);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.updateCampaignStatus(item, status);
            }
        });
    }

    updateCampaignStatus(item, status) {
        this.start();
        this.campaignService.status({ id: item.id, is_active: status }).subscribe(res => {
            this.getCampaignList(this.dateRange, 0);
        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    updateBid(item) {
        if (!item) return;

        const title = "Update Bid / Budget";
        const button = "Save Changes";
        const modal = this.modalService.show(UpdateCampaignComponent);
        (<UpdateCampaignComponent>modal.content).showCampaignModal(title, item, button);
        (<UpdateCampaignComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.getCampaignList(this.dateRange, 0);
            }
        });
    }

    start(): void {
        this.showLoading = true;
    }

    complete(): void {
        setTimeout(() => {
            this.showLoading = false;
        }, 500);
    }

    selectAll() {
        for (var i = 0; i < this.campaignList.length; i++) {
            this.campaignList[i].selected = this.selectedAll;
        }
    }

    checkIfAllSelected() {
        this.selectedAll = this.campaignList.every(function (item: any) {
            return item.selected == true;
        })
    }

    onChangeColumns(e: any) {
        this.storageService.set('campaign_columns', JSON.stringify(this.tableColumnModel));
    }

    hasColumn(name: string) {
        return _.includes(this.tableColumnModel, name);
    }
}