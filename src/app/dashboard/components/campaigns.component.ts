import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from './modals/confirm.component';
import { CreateCampaignComponent } from './modals/create-campaign.component';
import { CampaignService } from '../../shared/services/campaigns.service';
import { VoluumService } from '../../shared/services/voluum.service';

@Component({
    selector: 'dashboard-campaigns',
    templateUrl: './templates/campaigns.component.html',
})

export class CampaignsComponent implements OnInit, OnDestroy {
    title: string = "Manual Campaign";
    subscription: Subscription;
    campaignList: Array<any> = [];
    campaignsCount: number = 0;
    filterQuery: string;
    filterStatus: string = 'all';
    tableSetting = { rowsOnPage: 25, columnCount: 8, sortBy: "_id", sortOrder: "desc" };
    selectedAll: any;
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private messageService: MessageFlashService,
        private campaignService: CampaignService,
        private voluumService: VoluumService,
    ) { }

    ngOnInit() {
        this.list();
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    list() {
        this.subscriptionInit();
        this.subscription = this.campaignService.getAllCampaigns().subscribe(response => {
            this.campaignList = response;
            this.campaignsCount = Object.keys(response).length;
        }, err => {
            this.messageService.flashError(err);
        });
    }

    openCreateForm() {
        const title = "New Campaign";
        const button = "Create";
        this.showContentModal(title, null, button);
    }

    openEditForm(item) {
        const title = `Edit Campaign: ${item.campaignData.name}`;
        const button = "Save";
        this.showContentModal(title, item, button);
    }

    openDuplicateForm(item) {
        const title = `New Campaign (based on ${item.campaignData.name})`;
        const button = "Create";
        this.showContentModal(title, item, button);
    }

    showContentModal(title, body, button) {
        const modal = this.modalService.show(CreateCampaignComponent);
        (<CreateCampaignComponent>modal.content).showCampaignModal(title, body, button);
        (<CreateCampaignComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.list();
            }
        });
    }

    edit(item) {
        if (!item || !item.tracker_id) return;

        this.voluumService.getCampaignDetail(item.tracker_id).subscribe(response => {
            const data = {
                campaignData: item,
                campaignTracker: response
            };
            this.openEditForm(data);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    archive(item) {
        if (!item) return;

        const title = "Confirm Archive Campaign";
        const body = `Are you sure to archive campaign: ${item.name}?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.campaignService.archive(item._id).subscribe(res => {
                    this.messageService.flashSuccess(res.message);
                    this.list();
                }, err => {
                    this.messageService.flashError(err);
                });
            }
        });
    }

    unArchive(item) {
        if (!item) return;

        this.campaignService.unArchive(item._id).subscribe(res => {
            this.messageService.flashSuccess(res.message);
            this.list();
        }, err => {
            this.messageService.flashError(err);
        });
    }

    duplicate(item) {
        if (!item || !item.tracker_id) return;

        this.voluumService.getCampaignDetail(item.tracker_id).subscribe(response => {
            const data = {
                campaignData: item,
                campaignTracker: response,
                duplicate: true
            };
            this.openDuplicateForm(data);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    onChangeStatus() {
        switch (this.filterStatus) {
            case 'archived':
                this.campaignService.getArchives().subscribe(response => {
                    this.campaignList = response;
                    this.campaignsCount = Object.keys(response).length;
                }, err => {
                    this.messageService.flashError(err);
                });
                break;

            default:
                this.list();
                break;
        }
    }

    selectAll() {
        for (var i = 0; i < this.campaignList.length; i++) {
            this.campaignList[i].selected = this.selectedAll;
        }
    }

    checkIfAllSelected() {
        this.selectedAll = this.campaignList.every(function (item: any) {
            return item.selected == true;
        })
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}