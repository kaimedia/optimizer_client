import { Component, Input, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { ConfirmationModalComponent } from '../modals/confirm.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { StorageService } from '../../../core/services/storage.service';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { CampaignService } from '../../../shared/services/campaigns.service';
import { WidgetService } from '../../../shared/services/widgets.service';
import { SiteBidModalComponent } from '../modals/update-site-bid.component';
import { BlacklistModalComponent } from '../modals/blacklist-sites.component';

import * as _ from 'lodash';
import { stat } from 'fs';

@Component({
    selector: 'widget-sites',
    templateUrl: './templates/sites.component.html',
})

export class SitesComponent implements OnInit, OnDestroy {
    tableOptions = {
        rowsOnPage: 50,
        sortBy: "trk_clicks",
        sortOrder: "desc",
        columnCount: 50
    };

    tableColumnModel: any = [
        'site_name', 'bot', 'impressions', 'ctr', 'conversions', 'cost', 'revenue', 'net', 'roi',
        'clicks', 'trk_clicks', 'lp_clicks', 'lp_ctr', 'cpc', 'ap'];

    tableColumns: IMultiSelectOption[] = [
        { id: 'site_name', name: 'Site Name' },
        { id: 'bot', name: 'Bot' },
        { id: 'impressions', name: 'Impressions' },
        { id: 'clicks', name: 'TS Clicks' },
        { id: 'trk_clicks', name: 'Trk Clicks' },
        { id: 'lp_clicks', name: 'LP Clicks' },
        { id: 'ctr', name: 'CTR' },
        { id: 'lp_ctr', name: 'LP Ctr' },
        { id: 'cpc', name: 'CPC' },
        { id: 'conversions', name: 'Conversions' },
        { id: 'cost', name: 'Cost' },
        { id: 'revenue', name: 'Revenue' },
        { id: 'cpm', name: 'CPM' },
        { id: 'net', name: 'Net' },
        { id: 'roi', name: 'ROI' },
        { id: 'cv', name: 'CV' },
        { id: 'epc', name: 'EPC' },
        { id: 'epv', name: 'EPV' },
        { id: 'cpv', name: 'CPV' },
        { id: 'cpa', name: 'CPA' },
        { id: 'ap', name: 'AP' },
    ];

    filterQuery: string;
    selectedAll: any;
    disableBtnStatus: boolean = true;
    showBidding: boolean = false;
    actionValue: string = "";

    blockButton = {
        onText: "",
        offText: "",
        onColor: "red",
        offColor: "default",
        size: "small",
        disabled: false,
    };

    changeBidButton = {
        onText: "",
        offText: "",
        onColor: "red",
        offColor: "default",
        size: "small",
        disabled: false,
    };

    @Input() subscription;
    @Input() filterData;
    @Input() campaignId;
    @Input() campaignDetail;
    @Input() widgetList;
    @Input() widgetCount;
    @Input() statTotals;
    @Input() fraudCampaigns;
    @Input() fraudCampaignId;
    @Input() filterStatus;
    @Input() tableColumnSettings;
    @Input() tableColumnTexts;

    // Functions
    @Output() startLoading = new EventEmitter<string>();
    @Output() stopLoading = new EventEmitter<string>();
    @Output() addCampFraud = new EventEmitter<string>();
    @Output() cbFilterStatus = new EventEmitter<string>();

    constructor(
        private storageService: StorageService,
        private modalService: BsModalService,
        private messageFlashService: MessageFlashService,
        private widgetService: WidgetService,
        private campaignService: CampaignService,
    ) {

    }

    ngOnInit() {
        this.tableColumnModel = JSON.parse(this.storageService.get('site_columns')) || this.tableColumnModel;
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    onChangeColumns(e: any) {
        this.storageService.set('site_columns', JSON.stringify(this.tableColumnModel));
    }

    hasColumn(name: string) {
        return _.includes(this.tableColumnModel, name);
    }

    clearSearchText(): void {
        this.filterQuery = "";
    }

    getSitesByStatus(cache: boolean = true) {
        this.sitesByCampaign(this.filterData, cache);
        this.cbFilterStatus.next(this.filterStatus);
    }

    sitesByCampaign(filter: any, cache: boolean) {
        this.start();

        this.subscriptionInit();
        this.subscription = this.widgetService.siteMetricsByCampaign(filter).subscribe(
            results => {
                this.widgetList = results.sites;
                this.widgetCount = results.totalRows;
                this.blockButton.disabled = false;
                this.complete();
            },
            err => {
                this.messageFlashService.flashWarning(err);
                this.complete();
            }
        );
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    reloadDatas(): void {
        this.getSitesByStatus();
    }

    onBlockStatus(item) {
        if (item.status !== 'ACTIVE') {
            return true;
        }
        return false;
    }

    onChangeBlockStatus(status: boolean, item: any): void {
        let sites = [];
        sites.push(item);

        switch (status) {
            case true:
                this.requestBlockSites(sites);
                break;

            case false:
                this.requestUnBlockSites(sites);
                break;
        };
    }

    onAutoCpcStatus(item) {
        if (!item.auto_update_cpc) {
            return true;
        }
        return false;
    }

    onChangeAutoCpc(status: boolean, item: any): void {
        this.changeBidButton.disabled = true;
        this.widgetService.statusAutoChangeCpc(item, status).subscribe(res => {
            this.changeBidButton.disabled = false;
        }, err => {
            this.messageFlashService.flashError(err);
            this.changeBidButton.disabled = false;
        });
    }

    applyAction(): void {
        if (!this.actionValue)
            return;

        switch (this.actionValue) {
            case "active":
                this.enableSites();
                break;

            case "blocked":
                this.disableSites();
                break;

            case "pause":
                this.changeStatus(this.actionValue);
                break;

            case "whitelist":
            case "blacklist":
                this.changeStatus(this.actionValue);
                break;
        }
    }

    changeStatus(status: string) {
        const siteSelected = this.siteSelected();
        const siteSelectedNames = _.map(siteSelected, o => o.site_name);

        if (!this.isCheckedSite(siteSelected))
            return;

        if (status === 'blacklist' || status === 'pause') {
            const sitesNamesTxt = _.join(siteSelectedNames, '; ');
            const title = `Confirm ${status}`;
            const body = `Are you sure to ${status} selected sites: <b>${sitesNamesTxt}</b>?`;
            const modal = this.modalService.show(ConfirmationModalComponent);
            (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
            (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
                if (result === true) {
                    if (status === 'pause') {
                        this.requestBlockSites(this.setActionBlock(siteSelected));
                    } else {
                        this.changeStatusAction(status, this.setActionBlock(siteSelected));
                    }
                }
            });
        } else {
            this.changeStatusAction(status, siteSelected);
        }
    }

    changeStatusAction(status, siteSelected) {
        this.start();
        this.blockButton.disabled = true;
        this.widgetService.status({ sites: siteSelected }, status).subscribe(res => {
            this.messageFlashService.flashSuccess(res.message);
            this.getSitesByStatus(false);
        }, err => {
            this.messageFlashService.flashError(err);
            this.complete();
        });
    }

    disableSites() {
        const siteSelected = this.siteSelected();
        const siteSelectedNames = _.map(siteSelected, o => o.site_name);

        if (!this.isCheckedSite(siteSelected))
            return;

        const sitesNamesTxt = _.join(siteSelectedNames, '; ');
        const title = `Confirm Block Sites`;
        const body = `Are you sure to block selected sites: <b>${sitesNamesTxt}</b>?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.requestBlockSites(siteSelected);
            }
        });
    }

    enableSites() {
        const siteSelected = this.siteSelected();

        if (!this.isCheckedSite(siteSelected))
            return;

        this.requestUnBlockSites(siteSelected);
    }

    requestBlockSites(siteSelected) {
        this.start();
        this.blockButton.disabled = true;
        this.widgetService.blockSites(this.campaignId, { sites: this.setActionBlock(siteSelected) }).subscribe(res => {
            this.getSitesByStatus(false);
        }, err => {
            this.messageFlashService.flashError(err);
            this.complete();
        });
    }

    requestUnBlockSites(siteSelected) {
        this.start();
        this.blockButton.disabled = true;
        this.widgetService.unBlockSites(this.campaignId, { sites: siteSelected }).subscribe(res => {
            this.getSitesByStatus(false);
        }, err => {
            this.messageFlashService.flashError(err);
            this.complete();
        });
    }

    isCheckedSite(siteSelected) {
        if (!siteSelected || (siteSelected && _.isEmpty(siteSelected))) {
            this.messageFlashService.flashWarning("Select at least 1 row");
            return false;
        }
        return true;
    }

    siteSelected() {
        let siteSelected = _.filter(this.widgetList, o => { if (o.selected) return o });
        return siteSelected;
    }

    setActionBlock(sites) {
        if (_.size(sites) > 0) {
            sites = _.map(sites, item => {
                item.action = 'MANUAL';
                return item;
            });
        }
        return sites;
    }

    saveCampaignFraud(): void {
        this.addCampFraud.next(this.fraudCampaignId);
    }

    showCalcClicks(x: number, y: number): boolean {
        let subc = this.calcClicks(x, y);
        if ((subc != 0) && !isNaN(subc))
            return true;
        else
            return false;
    }

    renderCalcClicks(x: number, y: number): string {
        let subc = this.calcClicks(x, y);
        if (subc > 0)
            return ('+' + subc);
        else
            return subc;
    }

    calcClicks(x: number, y: number): any {
        return x - y;
    }

    showEditBidding(item): void {
        this.showBidding = true;
        const title = item.site_name;
        const modal = this.modalService.show(SiteBidModalComponent);
        (<SiteBidModalComponent>modal.content).showSiteBidModal(title, item, this.campaignDetail, this.filterData);
        (<SiteBidModalComponent>modal.content).onClose.subscribe(result => {
            if (result.status === true) {
                // this.getSitesByStatus(false);
            }
        });
    }

    cpcBidStatus(item) {
        let cpcBidTxt = `default ($${item.configured_cpc})`;
        if (item.cpc_boost !== 0 && item.cpc_boost !== "") {
            item.boosted_cpc = _.round(item.boosted_cpc, 3);
            cpcBidTxt = ((item.cpc_boost_percent > 0) ? "+" : "") + `${item.cpc_boost_percent}% ($${item.boosted_cpc})`;
        }
        return cpcBidTxt;
    }

    decreaseCpcBoost(item): void {
        this.messageFlashService.flashWarning("working this job...");
    }

    increaseCpcBoost(item): void {
        this.messageFlashService.flashWarning("working this job...");
    }

    displayBlockingLevel(item) {
        let blockText: string = "";
        switch (item.blocking_level) {
            case 'CAMPAIGN':
                blockText = "blocked";
                break;

            case 'ADVERTISER':
                blockText = "blocked on advertiser level";
                break;

            default:
                blockText = "blocked";
                break;
        }
        return blockText;
    }

    selectAll() {
        for (var i = 0; i < this.widgetCount; i++) {
            this.widgetList[i].selected = this.selectedAll;
        }

        this.checkIfHasSelected();
    }

    checkIfAllSelected() {
        this.selectedAll = this.widgetList.every(function (item: any) {
            return item.selected == true;
        });

        this.checkIfHasSelected();
    }

    checkIfHasSelected() {
        if (_.filter(this.widgetList, o => { if (o.selected) return o }).length > 0)
            this.disableBtnStatus = false;
        else
            this.disableBtnStatus = true;
    }

    start(): void {
        this.startLoading.next('start');
    }

    complete(): void {
        this.stopLoading.next('stop');
    }
}