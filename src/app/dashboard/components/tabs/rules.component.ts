import { Component, Input, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from '../modals/confirm.component';
import { CreateRuleComponent } from '../modals/create-rule.component';
import { RuleService } from '../../../shared/services/rule.service';

import * as _ from 'lodash';

@Component({
    selector: 'widget-rules',
    templateUrl: './templates/rules.component.html',
})

export class SiteRulesComponent implements OnInit, OnDestroy {
    subscription: Subscription;

    @Input() campaignDetail: any;
    @Input() ruleList: Array<any>;
    rulesCount: number = 0;

    selectedAll: any;
    filterQuery: string;
    tableSetting = { rowsOnPage: 25, columnCount: 8, sortBy: "created_at", sortOrder: "desc" };
    ruleActions = [null, 'Email Alert', 'Block', 'Block & Blacklist', 'Increase CPC', 'Decrease CPC'];
    levelInfo = [
        null,
        { icon: 'C', text: 'This rule will be executed on selected campaigns', type: 'success' },
        { icon: 'A', text: 'This rule will be executed on contents of selected campaigns', type: 'info' },
        { icon: 'W', text: 'This rule will be executed on sites of selected campaigns', type: 'primary' },
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageFlashService,
        private modalService: BsModalService,
        private ruleService: RuleService,
    ) { }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {
            this.list();
        });
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    list() {
        this.ruleService.list().subscribe(
            rules => {
                this.ruleList = _.filter(rules, o => _.includes(o.campaigns, this.campaignDetail._id));
                this.rulesCount = Object.keys(this.ruleList).length;
            },
            err => {
                this.messageService.flashError(err);
            });
    }

    showCriterias(item) {
        let criterias: string = '';

        let conditions = item.criterias;

        if (item.action === 4) {
            conditions = item.increase_cpc;
        }

        if (item.action === 5) {
            conditions = item.decrease_cpc;
        }

        conditions.forEach(element => {
            let operator;
            let calc;

            switch (element.operator) {
                case 'greater':
                    operator = '>';
                    break;

                case 'less':
                    operator = '<';
                    break;

                case 'equal':
                    operator = '=';
                    break;
            };

            switch (element.calc) {
                case 'add':
                    calc = '+';
                    break;

                case 'subtract':
                    calc = '-';
                    break;

                case 'multiply':
                    calc = '*';
                    break;

                case 'divide':
                    calc = '/';
                    break;
            };

            let elementValue = element.value;
            if (element.onCalc === true) {
                elementValue = `(${element.value}${calc}${element.xcolumn})`;
            }

            let value = `${elementValue}${element.unit}`;
            if (element.unit === '$') {
                value = `${element.unit}${elementValue}`;
            }

            criterias += `${element.column} ${operator} ${value}, `;
        });

        return criterias.slice(0, -2);
    }

    showLevel(item) {
        return {
            'icon': this.levelInfo[item.level]['icon'],
            'text': this.levelInfo[item.level]['text'],
            'type': this.levelInfo[item.level]['type'],
        };
    }

    showAction(item) {
        return this.ruleActions[item.action];
    }

    isEnalbe(item): boolean {
        if (item.status === true)
            return true;
        return false;
    }

    edit(item) {
        if (!item) return;
        this.openEditForm(item);
    }

    openCreateForm() {
        const title = "New Rule";
        const button = "Create";
        const extras = {
            campaign_id: this.campaignDetail._id
        };
        this.showContentModal(title, null, button, extras);
    }

    openEditForm(item) {
        const title = `Edit Rule: ${item.name}`;
        const button = "Save";
        this.showContentModal(title, item, button);
    }

    showContentModal(title, body, button, extras = null) {
        const modal = this.modalService.show(CreateRuleComponent);
        (<CreateRuleComponent>modal.content).showRuleModal(title, body, button, extras);
        (<CreateRuleComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.list();
            }
        });
    }

    selectAll() {
        for (var i = 0; i < this.ruleList.length; i++) {
            this.ruleList[i].selected = this.selectedAll;
        }
    }

    checkIfAllSelected() {
        this.selectedAll = this.ruleList.every(function (item: any) {
            return item.selected == true;
        })
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}