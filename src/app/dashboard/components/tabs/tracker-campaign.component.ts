import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subscription } from 'rxjs/Rx';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from '../modals/confirm.component';
import { CreateLanderComponent } from '../modals/create-lander.component';
import { CreateOfferComponent } from '../modals/create-offer.component';
import { VoluumService } from '../../../shared/services/voluum.service';

import * as _ from 'lodash';
@Component({
    selector: 'widget-tracker-campaign',
    templateUrl: './templates/tracker-campaign.component.html',
})

export class TrackerCampaignComponent implements OnInit, OnDestroy {
    subscription: Subscription;
    loading: boolean = false;
    @Input() campaignDetail: any;
    trafficSources: any;
    landers: any;
    offers: any;
    callbackCountry: string;

    switchButton = {
        onText: "On",
        offText: "Off",
        onColor: "green",
        offColor: "default",
        disabled: false,
        size: "normal",
    };

    campaign: any = {
        namePostfix: "",
        trafficSource: {
            id: ""
        },
        country: { //optional
            code: "US"
        },
        costModel: {
            type: "NOT_TRACKED"
        },
        redirectTarget: {
            inlineFlow: {
                defaultOfferRedirectMode: "REGULAR"
            },
        },
    };
    campaignURL: string;
    countPath: number = 1;
    defaultPaths = [
        {
            name: "Path 1",
            active: true,
            weight: 100,
            landers: [
                {
                    lander: {
                        id: ""
                    },
                    weight: 100
                },
            ],
            offers: [
                {
                    offer: {
                        id: ""
                    },
                    weight: 100
                },
            ],
            offerRedirectMode: "REGULAR",
            realtimeRoutingApiState: "DISABLED"
        },
    ];

    @Output() startLoading = new EventEmitter<string>();
    @Output() stopLoading = new EventEmitter<string>();

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private messageFlashService: MessageFlashService,
        private voluumService: VoluumService,
    ) {

    }

    ngOnInit() {
        this.callbackCountry = this.onChangeCountry.bind(this);

        this.loading = true;
        if (this.campaignDetail) {

            if (this.campaignDetail.tracker_id) {
                this.subscription = this.voluumService.getCampaignDetail(this.campaignDetail.tracker_id).subscribe(result => {
                    this.handlerCampaign(result);
                    this.multiData();
                }, err => {
                    this.messageFlashService.flashError(err);
                    this.loading = false;
                });
            } else {
                this.multiData();
            }

        }
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    onSubmit() {
        if (!this.canSubmit()) {
            this.messageFlashService.flashWarning("Please enter all required fields.");
            return;
        }

        this.campaign.redirectTarget.inlineFlow.name = `${this.campaign.namePostfix} - inline flow`;
        this.pushDefaultPathToCampaign();

        let obversable;
        if (this.campaign && this.campaign['id']) {
            obversable = this.voluumService.updateCampaign(this.campaign['id'], this.campaign);
        }

        this.start();
        obversable.subscribe(response => {
            this.campaign = this.campaign['id'] ? this.campaign : response;
            this.messageFlashService.flashSuccess("Successfully.");
            this.complete();
        }, err => {
            this.messageFlashService.flashError(err);
            this.complete();
        });
    }

    canSubmit() {
        if (!this.campaign.namePostfix || !this.campaign.trafficSource.id || !this.campaign.country.code || !this.campaign.costModel.type)
            return false;

        if (!this.defaultPaths[0].offers[0].offer.id)
            return this.messageFlashService.flashWarning("Select a least 1 offer.");

        return true;
    }

    multiData() {
        const getTrafficSources = this.voluumService.getTrafficSources();
        const getLanders = this.voluumService.getLanders();
        const getOffers = this.voluumService.getOffers();
        this.subscription = Observable.forkJoin(getTrafficSources, getLanders, getOffers).subscribe(
            results => {
                this.trafficSources = results[0].trafficSources;
                this.landers = _.filter(results[1].landers, ['country.code', this.campaign.country.code]);
                this.offers = _.filter(results[2].offers, ['country.code', this.campaign.country.code]);
                this.loading = false;
            },
            err => {
                this.messageFlashService.flashError(err);
                this.loading = false;
            }
        );
    }

    pushDefaultPathToCampaign() {
        _.forEach(this.defaultPaths, path => {
            path.offerRedirectMode = this.campaign.redirectTarget.inlineFlow.defaultOfferRedirectMode;
        });
        this.campaign.redirectTarget.inlineFlow.defaultPaths = this.defaultPaths;
    }

    handlerCampaign(campaignResult) {
        this.campaign = campaignResult;
        this.campaignURL = campaignResult.url;
        this.defaultPaths = campaignResult.redirectTarget.inlineFlow.defaultPaths;
    }

    onChangeCountry(countryCode) {
        const getLanders = this.voluumService.getLanders();
        const getOffers = this.voluumService.getOffers();

        this.subscription = Observable.forkJoin(getLanders, getOffers).subscribe(
            results => {
                this.landers = _.filter(results[0].landers, ['country.code', countryCode]);
                this.offers = _.filter(results[1].offers, ['country.code', countryCode]);;
            },
            err => {
                this.messageFlashService.flashError(err);
            }
        );
    }

    onFlagChange(status: boolean, path) {
        path.active = status;
    }

    //path
    addPath() {
        this.countPath = Object.keys(this.defaultPaths).length;
        this.defaultPaths.push({
            name: `Path ${this.countPath + 1}`,
            active: true,
            weight: 100,
            landers: [
                {
                    lander: {
                        id: ""
                    },
                    weight: 100
                },
            ],
            offers: [
                {
                    offer: {
                        id: ""
                    },
                    weight: 100
                },
            ],
            offerRedirectMode: "REGULAR",
            realtimeRoutingApiState: "DISABLED"
        });
    }

    removePath(pathIndex) {
        if (Object.keys(this.defaultPaths).length == 1) {
            return this.messageFlashService.flashWarning("Can't remove all path list.");
        }

        this.defaultPaths.splice(pathIndex, 1);
    }

    // lander
    addLander(path) {
        path.landers.push({
            lander: {
                id: ""
            },
            weight: 100
        });
    }

    removeLander(landerIndex, path) {
        path.landers.splice(landerIndex, 1);
    }

    newLander() {
        const title = "New Lander";
        const button = "Create";
        const extras = {
            country: this.campaign.country.code
        };
        this.showLanderPopup(title, null, button, extras);
    }

    editLander(landers) {
        const landerId = landers.lander.id;

        if (!landerId)
            return this.messageFlashService.flashWarning("Select a lander.");

        this.subscription = this.voluumService.getLander(landerId).subscribe(lander => {
            const title = `Edit Lander: ${lander.name}`;
            const button = "Save Changes";
            this.showLanderPopup(title, lander, button);
        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    showLanderPopup(title, lander, button, extras = null) {
        const modal = this.modalService.show(CreateLanderComponent);
        (<CreateLanderComponent>modal.content).showLanderModal(title, lander, button, extras);
        (<CreateLanderComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.requestLanderList();
            }
        });
    }

    requestLanderList() {
        this.subscription = this.voluumService.getLanders().subscribe(result => {
            this.landers = result.landers;
        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    //offer
    addOffer(path) {
        path.offers.push({
            offer: {
                id: ""
            },
            weight: 100
        });
    }

    removeOffer(offerIndex, path) {
        if (Object.keys(path.offers).length == 1) {
            return this.messageFlashService.flashWarning("Can't remove all offer.");
        }
        path.offers.splice(offerIndex, 1);
    }

    newOffer() {
        const title = "New Offer";
        const button = "Create";
        const extras = {
            country: this.campaign.country.code
        };
        this.showOfferPopup(title, null, button, extras);
    }

    editOffer(offers) {
        const offerId = offers.offer.id;

        if (!offerId)
            return this.messageFlashService.flashWarning("Select a offer.");

        this.subscription = this.voluumService.getOffer(offerId).subscribe(offer => {
            const title = `Edit Offer: ${offer.name}`;
            const button = "Save Changes";
            this.showOfferPopup(title, offer, button);
        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    showOfferPopup(title, offer, button, extras = null) {
        const modal = this.modalService.show(CreateOfferComponent);
        (<CreateOfferComponent>modal.content).showOfferModal(title, offer, button, extras);
        (<CreateOfferComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.requestOfferList();
            }
        });
    }

    requestOfferList() {
        this.subscription = this.voluumService.getOffers().subscribe(result => {
            this.offers = result.offers;
        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    start(): void {
        this.startLoading.next('start');
    }

    complete(): void {
        setTimeout(() => {
            this.stopLoading.next('stop');
        }, 300);
    }

}