import { Component, Input, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { StorageService } from '../../../core/services/storage.service';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { VoluumService } from '../../../shared/services/voluum.service';

import * as _ from 'lodash';

@Component({
    selector: 'widget-tracker-view',
    templateUrl: './templates/tracker-view.component.html',
})

export class TrackerViewComponent implements OnInit, OnDestroy {
    subscription: Subscription;
    loading: boolean = false;

    tableOptions = {
        rowsOnPage: 25,
        sortBy: "visits",
        sortOrder: "desc",
        columnCount: 12
    };

    tableColumnModel: any = ['weight', 'clicks', 'visits', 'ctr', 'conversions', 'cost', 'revenue', 'roi', 'profit', 'epc', 'epv'];

    tableColumns: IMultiSelectOption[] = [
        { id: 'weight', name: 'Weight' },
        { id: 'clicks', name: 'Clicks' },
        { id: 'visits', name: 'Visits' },
        { id: 'ctr', name: 'CTR' },
        { id: 'conversions', name: 'Conversions' },
        { id: 'cost', name: 'Cost' },
        { id: 'revenue', name: 'Revenue' },
        { id: 'profit', name: 'Profit' },
        { id: 'epc', name: 'EPC' },
        { id: 'roi', name: 'ROI' },
        { id: 'epv', name: 'EPV' },
        { id: 'ap', name: 'AP' },
    ];

    @Input() filterData;
    @Input() campaignDetail;
    @Input() trackerView;
    @Input() tableColumnSettings;
    @Input() tableColumnTexts;

    @Output() startLoading = new EventEmitter<string>();
    @Output() stopLoading = new EventEmitter<string>();

    constructor(
        private storageService: StorageService,
        private messageService: MessageFlashService,
        private voluumService: VoluumService,
    ) {

    }

    ngOnInit() {
        this.loading = true;

        if (this.trackerView)
            this.loading = false;

        this.tableColumnModel = JSON.parse(this.storageService.get('tracker_view_columns')) || this.tableColumnModel;
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    onChangeColumns(e: any) {
        this.storageService.set('tracker_view_columns', JSON.stringify(this.tableColumnModel));
    }

    hasColumn(name: string) {
        return _.includes(this.tableColumnModel, name);
    }

    reloadDatas(): void {
        this.start();
        this.subscriptionInit();
        this.subscription = this.voluumService.trackerView(this.campaignDetail.tracker_id, this.filterData).subscribe(response => {
            this.trackerView = response;
            this.complete();
        }, err => {
            this.messageService.flashError(err);
            this.complete();
        });
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    start(): void {
        this.startLoading.next('start');
    }

    complete(): void {
        this.stopLoading.next('stop');
    }
}