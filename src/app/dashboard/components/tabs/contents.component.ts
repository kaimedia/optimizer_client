import { Component, Input, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { StorageService } from '../../../core/services/storage.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from '../modals/confirm.component';
import { ContentService } from '../../../shared/services/content.service';

import * as _ from 'lodash';

@Component({
    selector: 'widget-contents',
    templateUrl: './templates/contents.component.html',
})

export class ContentsComponent implements OnInit, OnDestroy {
    subscription: Subscription;

    tableOptions = {
        rowsOnPage: 50,
        sortBy: "trk_clicks",
        sortOrder: "desc",
        columnCount: 50
    };

    tableColumnModel: any = [
        'id', 'impressions', 'clicks', 'trk_clicks', 'lp_clicks', 'lp_ctr', 'ctr', 'cpc', 'conversions', 'cost', 'revenue', 'net', 'roi'];

    tableColumns: IMultiSelectOption[] = [
        { id: 'id', name: 'Content ID' },
        { id: 'impressions', name: 'Impressions' },
        { id: 'clicks', name: 'Clicks' },
        { id: 'trk_clicks', name: 'Trk Clicks' },
        { id: 'lp_clicks', name: 'LP Clicks' },
        { id: 'ctr', name: 'CTR' },
        { id: 'lp_ctr', name: 'LP Ctr' },
        { id: 'cpc', name: 'CPC' },
        { id: 'conversions', name: 'Conversions' },
        { id: 'cost', name: 'Cost' },
        { id: 'revenue', name: 'Revenue' },
        { id: 'cpm', name: 'CPM' },
        { id: 'net', name: 'Net' },
        { id: 'roi', name: 'ROI' },
        { id: 'cv', name: 'CV' },
        { id: 'epc', name: 'EPC' },
        { id: 'epv', name: 'EPV' },
        { id: 'cpv', name: 'CPV' },
        { id: 'cpa', name: 'CPA' },
        { id: 'ap', name: 'AP' },
    ];

    selectedAll;
    filterQuery: string;
    disableBtnStatus: boolean = true;

    @Input() filterData;
    @Input() contentList;
    @Input() contentSummary;
    @Input() contentCount;
    @Input() campaignId;

    @Input() tableColumnSettings;
    @Input() tableColumnTexts;

    @Output() startLoading = new EventEmitter<string>();
    @Output() stopLoading = new EventEmitter<string>();

    constructor(
        private messageService: MessageFlashService,
        private storageService: StorageService,
        private modalService: BsModalService,
        private contentService: ContentService,
    ) {

    }

    ngOnInit() {
        this.tableColumnModel = JSON.parse(this.storageService.get('content_columns')) || this.tableColumnModel;
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    list(cache: number = 1) {
        this.subscriptionInit();
        this.subscription = this.contentService.contentMetrics(this.campaignId, this.filterData).subscribe(
            res => {
                this.contentList = res.contents;
                this.contentCount = res.totalRows;
                if (res.summary) {
                    this.contentSummary = res.summary;
                }
                this.complete();
            },
            err => {
                this.messageService.flashWarning(err);
            }
        );
    }

    onChangeColumns(e: any) {
        this.storageService.set('content_columns', JSON.stringify(this.tableColumnModel));
    }

    hasColumn(name: string) {
        return _.includes(this.tableColumnModel, name);
    }

    clearSearchText(): void {
        this.filterQuery = "";
    }

    showOnOff(item) {
        let enabled = "Off";
        if (item.is_active === true)
            enabled = "On";
        return enabled;
    }

    showCalcClicks(x: number, y: number): boolean {
        let subc = this.calcClicks(x, y);
        if ((subc != 0) && !isNaN(subc))
            return true;
        else
            return false;
    }

    renderCalcClicks(x: number, y: number): string {
        let subc = this.calcClicks(x, y);
        if (subc > 0)
            return ('+' + subc);
        else
            return subc;
    }

    calcClicks(x: number, y: number): any {
        return x - y;
    }

    showItemId(item: any) {
        return item.id;
    }

    selectAll() {
        for (var i = 0; i < this.contentCount; i++) {
            this.contentList[i].selected = this.selectedAll;
        }

        this.checkIfHasSelected();
    }

    checkIfAllSelected() {
        this.selectedAll = this.contentList.every(function (item: any) {
            return item.selected == true;
        });

        this.checkIfHasSelected();
    }

    checkIfHasSelected() {
        if (_.filter(this.contentList, o => { if (o.selected) return o }).length > 0)
            this.disableBtnStatus = false;
        else
            this.disableBtnStatus = true;
    }

    isEnalbe(item): boolean {
        if (item.is_active === true)
            return true;
        return false;
    }

    enable(item) {
        if (!item) return;
        this.status(item, true);
    }

    disable(item) {
        if (!item) return;

        const title = "Confirm Disable";
        const body = `Ad name: <b>${item.title}</b>?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.status(item, false);
            }
        });
    }

    status(item: any, value: boolean) {
        this.start();

        this.contentService.update(this.campaignId, item.id, { is_active: value }).subscribe(res => {
            this.messageService.flashSuccess(res.message);
            item.is_active = value;
            this.complete();
        }, err => {
            this.messageService.flashError(err);
            this.complete();
        });
    }

    delete(item) {
        if (!item) return;

        const title = "Confirm Delete";
        const body = `Ad name: <b>${item.title}</b>?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.start();
                this.contentService.remove(this.campaignId, item.id).subscribe(res => {
                    this.messageService.flashSuccess(res.message);
                    this.list(0);
                    this.complete();
                }, err => {
                    this.messageService.flashError(err);
                    this.complete();
                });
            }
        });
    }

    removeItem(item: any) {
        this.contentList = _.filter(this.contentList, (elem) => elem != item);
    }

    reloadDatas(): void {
        this.start();
        this.list();
    }

    start(): void {
        this.startLoading.next('start');
    }

    complete(): void {
        this.stopLoading.next('stop');
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

}