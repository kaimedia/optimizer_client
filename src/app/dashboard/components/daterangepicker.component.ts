import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DaterangepickerConfig, DaterangePickerComponent } from 'ng2-daterangepicker';
import { DateRangeService } from "../../shared/services/date-range.service";
import { environment } from '../../../environments/environment';
import * as moment from 'moment-timezone';

const dateFormat = 'YYYY-MM-DD';
moment.tz.setDefault(environment.timeZone);

@Component({
    selector: 'daterangepicker',
    template: `
        <div id="dashboard-date-range" class="input-group" daterangepicker (selected)="selectedDate($event, mainInput)">
            <span class="form-control uneditable-input" name="daterange">
                <i class="icon-calendar"></i>
                {{ mainInput.start_date | date:'MMMM dd, y' }} - {{ mainInput.end_date | date:'MMMM dd, y' }}
            </span>
        </div>
`
})

export class DateRangeComponent implements OnInit {
    @ViewChild(DaterangePickerComponent)
    @Input() callback;

    picker: DaterangePickerComponent;
    dateRange: any = {};
    mainInput = {
        start_date: moment().format(dateFormat),
        end_date: moment().format(dateFormat)
    };

    constructor(
        private daterangepickerOptions: DaterangepickerConfig,
        private dateRangeService: DateRangeService,
    ) {
        this.daterangepickerOptions.settings = {
            opens: "left",
            alwaysShowCalendars: false,
            minDate: '2017-01-01',
            maxDate: moment().format(dateFormat),
            // dateLimit: {
            //     days: 30
            // },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 3 Days': [moment().subtract(2, 'days'), moment()],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'months').startOf('month'), moment().subtract(1, 'months').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Lifetime': [moment('2017-01-01'), moment()],
            },
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: 'Apply',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
    }

    ngOnInit() {
        const dateLocal = this.dateRangeService.get() || this.mainInput;
        this.mainInput = dateLocal;
        this.initDate(dateLocal);
    }

    public selectedDate(value: any, dateInput: any) {
        let from = moment(value.start).format(dateFormat);
        let to = moment(value.end).format(dateFormat);

        dateInput.start_date = from;
        dateInput.end_date = to;

        this.dateRange.start_date = from;
        this.dateRange.end_date = to;

        this.dateRangeService.set(this.dateRange);
        this.callback(this.dateRange);
    }

    private initDate(dateInit: any) {
        this.daterangepickerOptions.settings.startDate = dateInit.start_date;
        this.daterangepickerOptions.settings.endDate = dateInit.end_date;
    }

    public calendarEventsHandler(e: any) {

    }

}