import { Component, Input} from '@angular/core';

@Component({
    selector: 'dashboard-stats',
    templateUrl: './templates/stats.component.html'
})

export class StatsComponent {
    @Input() statTotals;
}