import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from './modals/confirm.component';
import { CreateTrackerComponent } from './modals/create-tracker.component';
import { AccountsService } from '../../shared/services/accounts.service';
import { TrackerService } from '../../shared/services/tracker.service';

@Component({
    selector: 'dashboard-trackers',
    templateUrl: './templates/tracker.component.html',
})

export class TrackerComponent implements OnInit, OnDestroy {
    title: string = "Tracker Accounts";
    @Input() trackerList: Array<any>;
    trackersCount: number = 0;
    filterQuery: string;
    tableSetting = { rowsOnPage: 5, columnCount: 6, sortBy: "created_at", sortOrder: "desc" };
    selectedAll: any;
    subscription: Subscription;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private messageService: MessageFlashService,
        private accountsService: AccountsService,
        private trackerService: TrackerService,
    ) { }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {
            this.list();
        });
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    list() {
        this.subscriptionInit();
        this.subscription = this.trackerService.list().subscribe(
            trackers => {
                trackers = JSON.parse(new Buffer(JSON.stringify(trackers), 'base64').toString());
                this.trackerList = trackers;
                this.trackersCount = Object.keys(trackers).length;
            },
            err => {
                this.messageService.flashError(err);
            });
    }

    edit(item) {
        if (!item) return;
        this.openEditForm(item);
    }

    enable(item) {
        if (!item) return;
        this.status(item._id, true);
    }

    disable(item) {
        if (!item) return;

        const title = "Confirm Disable";
        const body = `Are you sure to disable tracker: ${item.name}?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.status(item._id, false);
                // this.accountsService.remove();
            }
        });
    }

    delete(item) {
        if (!item) return;

        const title = "Confirm Delete";
        const body = `Are you sure to delete tracker: ${item.name}?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.trackerService.remove(item._id).subscribe(res => {
                    this.list();
                    // this.accountsService.remove();
                }, err => {
                    this.messageService.flashError(err);
                });
            }
        });
    }

    status(id: string, value: boolean) {
        let type: string;
        if (value === true)
            type = "Enable";
        else
            type = "Disable";

        this.trackerService.update(id, { status: value }).subscribe(res => {
            this.list();
            this.messageService.flashSuccess(`${type} tracker successfully.`);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    isEnalbe(item): boolean {
        if (item.status === true)
            return true;
        return false;
    }

    displayKeyId(item) {
        return `*******************${item.access_id.slice(-10)}`;
    }

    openCreateForm() {
        const title = "Add New Tracker";
        const button = "Create";
        this.showContentModal(title, null, button);
    }

    openEditForm(item) {
        const title = `Edit Tracker: ${item.name}`;
        const button = "Save";
        this.showContentModal(title, item, button);
    }

    showContentModal(title, body, button) {
        const modal = this.modalService.show(CreateTrackerComponent);
        (<CreateTrackerComponent>modal.content).showTrackerModal(title, body, button);
        (<CreateTrackerComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.list();
            }
        });
    }

    selectAll() {
        for (var i = 0; i < this.trackerList.length; i++) {
            this.trackerList[i].selected = this.selectedAll;
        }
    }

    checkIfAllSelected() {
        this.selectedAll = this.trackerList.every(function (item: any) {
            return item.selected == true;
        })
    }
}