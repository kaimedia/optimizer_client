import { Component, OnInit, Input } from '@angular/core';

import { environment } from '../../../environments/environment';

import * as moment from 'moment';

const dateFormat = 'dddd, DD MMMM YYYY, HH:mm A Z';
const tz = environment.timeZone;

@Component({
    selector: 'system-time',
    template: `
    <div class="pull-right" style="cursor: default;">
        <span class="thin uppercase hidden-xs">{{systemTime}}</span>
    </div>
    <div class="clearfix"></div>
`
})

export class SystemTimeComponent implements OnInit {
    @Input() systemTime: string;

    constructor() { }

    ngOnInit() {
        this.systemTime = moment().utcOffset(tz).format(dateFormat);
    }
}