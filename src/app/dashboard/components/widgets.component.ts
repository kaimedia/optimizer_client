import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subscription } from 'rxjs/Rx';
import { IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { environment } from '../../../environments/environment';
import { AccountsService } from '../../shared/services/accounts.service';
import { RoleService } from '../../shared/services/role.service';
import { DateRangeService } from "../../shared/services/date-range.service";
import { CampaignService } from '../../shared/services/campaigns.service';
import { WidgetService } from '../../shared/services/widgets.service';
import { ContentService } from '../../shared/services/content.service';
import { VoluumService } from '../../shared/services/voluum.service';
import { FraudService } from '../../shared/services/fraud.service';

import * as moment from "moment-timezone";
import * as _ from 'lodash';

const dateFormat = 'YYYY-MM-DD';
const timerRefresh = environment.refreshTime;
moment.tz.setDefault(environment.timeZone);

@Component({
    selector: 'dashboard-widgets',
    templateUrl: './templates/widgets.component.html',
})

export class WidgetsComponent implements OnInit, OnDestroy {
    @ViewChild("statsChart") statsChart: BaseChartDirective;
    @ViewChild("blockChart") blockChart: BaseChartDirective;

    public subscription: Subscription;
    private timerSubscription;
    systemTime: string;
    showLoading: boolean = false;
    widgetList: any;
    widgetCount: number = 0;
    contentList: any;
    contentCount: number = 0;
    contentSummary: object = {};
    statTotals: any;
    filterDate: Object;
    filterData: any;
    filterStatus: string = "active";
    campaignId: any;
    campaignIdDb: string;
    campaignDetail: any;
    fraudCampaigns: Array<any>;
    fraudCampaignId: string;
    trackerView: any;

    dateRangeDefault: any = {
        start_date: moment().format(dateFormat),
        end_date: moment().format(dateFormat)
    };

    statsChartData: Array<any>;
    statsChartLabels: Array<any>;
    statsChartColors: Array<any> = [{
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.1)',
    }, {
        borderColor: 'rgb(54, 162, 235)',
        backgroundColor: 'rgba(54, 162, 235, 0.1)',
    }];
    statsChartOptions: any = {
        responsive: true
    };
    statsChartLegend: boolean = true;
    statsChartType: string = 'line';

    blockChartLabels: string[];
    blockChartData: number[];
    blockChartType: string = 'pie';

    accountAPI: any;
    trafficSourceLogo: string;
    trackerLogo: string;

    tableColumnSettings: IMultiSelectSettings = {
        fixedTitle: true,
        checkedStyle: 'glyphicon',
        buttonClasses: 'btn btn-primary',
        displayAllSelectedText: true,
        pullRight: true,
        showCheckAll: true,
        showUncheckAll: true,
        maxHeight: '400px',
    };

    tableColumnTexts: IMultiSelectTexts = {
        defaultTitle: 'Columns',
    };

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private roleService: RoleService,
        private dateRangeService: DateRangeService,
        private messageFlashService: MessageFlashService,
        private accountsService: AccountsService,
        private widgetService: WidgetService,
        private campaignService: CampaignService,
        private contentService: ContentService,
        private voluumService: VoluumService,
        private fraudService: FraudService,
    ) { }

    ngOnInit() {
        this.filterDate = this.dateFilterResult.bind(this);
        this.filterData = this.dateRangeService.get() || this.dateRangeDefault;

        this.activatedRoute.parent.parent.data.subscribe((data: { user }) => {
            this.roleService.addRole(data.user.role);
        });

        this.activatedRoute.params.subscribe(params => {
            this.subscriptionInit();
            this.subscription = this.campaignService.detail(params['id']).subscribe(result => {
                if (result.id) {
                    this.displayAccounts();
                    this.campaignId = result.id;
                    this.campaignIdDb = result._id ? result._id : result.id ? result.id : "";
                    this.campaignDetail = result;
                    this.fraudCampaignId = result.id_fraud || "";
                    this.filterData.campaign_id = this.campaignId
                    this.multipleData(this.filterData);
                    this.subscribeTimerInit();
                } else {
                    this.router.navigate(['/dashboard']);
                }
            }, err => {
                this.messageFlashService.flashError(err);
            });
        });
    }

    ngOnDestroy() {
        this.subscriptionInit();
        if (this.timerSubscription) {
            this.timerSubscription.unsubscribe();
        }
    }

    dateFilterResult(newDateRange: Object) {
        this.filterData['start_date'] = newDateRange['start_date'];
        this.filterData['end_date'] = newDateRange['end_date'];
        this.multipleData(this.filterData);
    }

    refreshSystemTime() {
        this.systemTime = moment().format('dddd, DD MMMM YYYY, HH:mm A Z');
    }

    multipleData(filter: any, cache: number = 1) {
        this.start();
        this.refreshSystemTime();

        let getWidgets = this.widgetService.siteMetricsByCampaign(filter);
        let getFraudCampaignStats = this.campaignService.fraudStatsCampaign(this.campaignId, filter);
        let getContents = this.contentService.contentMetrics(this.campaignId, filter);

        let promises = [getWidgets, getFraudCampaignStats, getContents];
        if (this.campaignDetail && this.campaignDetail.tracker_id) {
            promises.push(this.voluumService.trackerView(this.campaignDetail.tracker_id, this.filterData));
        }

        this.subscriptionInit();
        this.subscription = Observable.forkJoin(promises).subscribe(
            results => {
                this.statTotals = results[0].summary;
                this.widgetList = results[0].sites;
                this.widgetCount = results[0].totalRows;
                this.handlerFraudData(results[1].data);
                this.contentList = results[2].contents;
                this.contentCount = results[2].totalRows;
                if (results[2].summary) {
                    this.contentSummary = results[2].summary;
                }

                if (results[3]) {
                    this.trackerView = results[3];
                }

                this.complete();
            },
            err => {
                this.messageFlashService.flashWarning(err);
                this.complete();
            }
        );
    }

    getTrackerView() {
        if (this.campaignDetail && this.campaignDetail.tracker_id) {
            this.voluumService.trackerView(this.campaignDetail.tracker_id, this.filterData).subscribe(result => {
                this.trackerView = result;
            }, err => {
                this.messageFlashService.flashError(err);
            });
        }
    }

    getCampaignsFraud() {
        this.fraudService.list().subscribe(result => {
            this.fraudCampaigns = result;
        }, err => {
            this.messageFlashService.flashError(err);
        });
    }

    displayAccounts() {
        this.accountAPI = this.accountsService.get();
        this.getTrafficSourceLogo(this.accountAPI.traffic_source);
        this.getTrackerLogo(this.accountAPI.tracker);
    }

    getTrafficSourceLogo(traffic_source) {
        this.trafficSourceLogo = `assets/images/traffic_source_logo/${_.toLower(traffic_source.type)}.png`;
    }

    getTrackerLogo(tracker) {
        this.trackerLogo = `assets/images/tracker_logo/${_.toLower(tracker.type)}.png`;
    }

    subscribeTimerInit() {
        this.timerSubscription = Observable.interval(timerRefresh).subscribe(() => this.multipleData(this.filterData, 0));
    }

    subscribeTimerToData() {
        this.timerSubscription = Observable.timer(timerRefresh).first().subscribe(() => this.multipleData(this.filterData, 0));
    }

    reloadAllData() {
        this.multipleData(this.filterData, 0);
    }

    handlerFraudData(stats) {
        if (!_.isEmpty(stats)) {
            let totals = _.map(stats.daily, o => o.total);
            let blocked = _.map(stats.daily, o => o.secondary);

            this.statsChartData = [
                { data: blocked, label: 'Blocked' },
                { data: totals, label: 'Total' }
            ];
            this.statsChartLabels = _.map(stats.daily, o => o.date);
            this.reloadChart(this.statsChart, this.statsChartData, this.statsChartLabels);

            if (_.size(stats.total) > 0) {
                this.blockChartData = [stats.total.underreview, stats.total.filter];
                this.blockChartLabels = ['Under Review', 'Filters'];
                this.reloadChart(this.blockChart, this.blockChartData, this.blockChartLabels);
            }

            /**
             * Add Total Bot column
             */
            this.statTotals.total_bot = stats.total.total;
            this.statTotals.block_bot = stats.total.secondary;
            this.statTotals.bot = this.calcBotPercent(this.statTotals.block_bot, this.statTotals.total_bot);
            this.statTotals.bot = _.round((this.statTotals.bot * 100), 2);
        }
    }

    calcBotPercent(block_bot, total_bot) {
        if (total_bot === 0 || isNaN(total_bot))
            return 0;
        else
            return _.divide(block_bot, total_bot);
    }

    reloadChart(chart, data, labels) {
        if (chart !== undefined) {
            if (!_.isUndefined(chart.datasets)) {
                setTimeout(() => {
                    chart.datasets = data;
                }, 300);
            }

            if (!_.isUndefined(chart.data)) {
                setTimeout(() => {
                    chart.data = data;
                }, 300);
            }

            chart.labels = labels;
            chart.ngOnInit();
        }
    }

    saveCampaignFraud(campaignFraud_id) {
        if (!campaignFraud_id)
            return;

        this.start();
        this.campaignService.campaignApiUpdate({ _id: this.campaignIdDb, id_fraud: campaignFraud_id }).subscribe(res => {
            this.multipleData(this.filterData, 0);
            this.messageFlashService.flashSuccess("Connected bot campaign successfully.");
        }, err => {
            this.messageFlashService.flashError(err);
            this.complete();
        });
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    callBackfilterStatus(status) {
        this.filterStatus = status;
    }

    addKeyValues(key: string, value: any) {
        this.filterData[key] = value;
    }

    removeKey(object: Object, keys: Array<any>) {
        return _.omit(object, keys)
    }

    start(): void {
        this.showLoading = true;
    }

    complete(): void {
        setTimeout(() => {
            this.showLoading = false;
        }, 300);
    }

}