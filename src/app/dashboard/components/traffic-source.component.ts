import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from './modals/confirm.component';
import { CreateTrafficSourceComponent } from './modals/create-traffic-source.component';
import { AccountsService } from '../../shared/services/accounts.service';
import { TrafficSourceService } from '../../shared/services/traffic-source.service';

import * as _ from 'lodash';

@Component({
    selector: 'dashboard-traffic-sources',
    templateUrl: './templates/traffic-source.component.html',
})

export class TrafficSourceComponent implements OnInit, OnDestroy {
    title: string = "Traffic Sources";
    @Input() trafficSourceList: Array<any>;
    trafficSourcesCount: number = 0;
    filterQuery: string;
    tableSetting = { rowsOnPage: 5, columnCount: 7, sortBy: "created_at", sortOrder: "desc" };
    selectedAll: any;
    subscription: Subscription;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private messageService: MessageFlashService,
        private accountsService: AccountsService,
        private trafficSourceService: TrafficSourceService,
    ) { }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {
            this.list();
        });
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    list() {
        this.subscriptionInit();
        this.subscription = this.trafficSourceService.list().subscribe(
            trafficSources => {
                trafficSources = JSON.parse(new Buffer(JSON.stringify(trafficSources), 'base64').toString());
                this.trafficSourceList = trafficSources;
                this.trafficSourcesCount = Object.keys(trafficSources).length;
            },
            err => {
                this.messageService.flashError(err);
            });
    }

    edit(item) {
        if (!item) return;
        this.openEditForm(item);
    }

    enable(item) {
        if (!item) return;
        this.status(item._id, true);
    }

    disable(item) {
        if (!item) return;

        const title = "Confirm Disable";
        const body = `Are you sure to disable traffic source: ${item.name}?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.status(item._id, false);
                this.accountsService.remove();
            }
        });
    }

    delete(item) {
        if (!item) return;

        const title = "Confirm Delete";
        const body = `Are you sure to delete traffic source: ${item.name}?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.trafficSourceService.remove(item._id).subscribe(res => {
                    this.list();
                    this.accountsService.remove();
                }, err => {
                    this.messageService.flashError(err);
                });
            }
        });
    }

    status(id: string, value: boolean) {
        let type: string;
        if (value === true)
            type = "Enable";
        else
            type = "Disable";
        this.trafficSourceService.update(id, { status: value }).subscribe(res => {
            this.list();
            this.messageService.flashSuccess(`${type} traffic source successfully.`);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    isEnalbe(item): boolean {
        if (item.status === true)
            return true;
        return false;
    }

    displayKeyId(key) {
        return `*******************${key.slice(-10)}`;
    }

    openCreateForm() {
        const title = "Add New Traffic Source";
        const button = "Create";
        this.showContentModal(title, null, button);
    }

    openEditForm(item) {
        const title = `Edit Traffic Source: ${item.name}`;
        const button = "Save";
        this.showContentModal(title, item, button);
    }

    showContentModal(title, body, button) {
        const modal = this.modalService.show(CreateTrafficSourceComponent);
        (<CreateTrafficSourceComponent>modal.content).showTrafficSourceModal(title, body, button);
        (<CreateTrafficSourceComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.list();
            }
        });
    }

    selectAll() {
        for (var i = 0; i < this.trafficSourceList.length; i++) {
            this.trafficSourceList[i].selected = this.selectedAll;
        }
    }

    checkIfAllSelected() {
        this.selectedAll = this.trafficSourceList.every(function (item: any) {
            return item.selected == true;
        })
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}