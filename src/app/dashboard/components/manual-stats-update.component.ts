import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { DaterangepickerConfig, DaterangePickerComponent } from 'ng2-daterangepicker';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { CampaignService, Campaign } from '../../shared/services/campaigns.service';
import { WidgetService, Site } from '../../shared/services/widgets.service';
import { ContentService } from '../../shared/services/content.service';
import { environment } from '../../../environments/environment';
import * as moment from 'moment-timezone';
import * as _ from 'lodash';

const dateFormat = 'YYYY-MM-DD';
moment.tz.setDefault(environment.timeZone);

@Component({
    selector: 'dashboard-manual-stats-update',
    templateUrl: './templates/manual-stats-update.component.html',
})

export class ManualStatsUpdateComponent implements OnInit, OnDestroy {
    title: string = "Manual Stats Update";
    showLoading: Boolean = false;
    data: any = {
        campaign_id: '',
        start_date: moment().format(dateFormat),
        end_date: moment().format(dateFormat),
    };
    campaigns: Array<any> = [];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageFlashService,
        private daterangepickerOptions: DaterangepickerConfig,
        private campaignService: CampaignService,
        private siteService: WidgetService,
        private contentService: ContentService,
    ) {
        this.daterangepickerOptions.settings = {
            opens: "right",
            alwaysShowCalendars: true,
            minDate: '2017-01-01',
            maxDate: moment().format(dateFormat),
            dateLimit: {
                days: 10
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 3 Days': [moment().subtract(2, 'days'), moment()],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            },
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: 'Apply',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
    }

    ngOnInit() {
        this.campaignList();
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        this.showLoading = true;
        let updateStatsCampaign = this.campaignService.updateStats(this.data.campaign_id, this.data.start_date, this.data.end_date);
        let updateStatsSites = this.siteService.updateStats(this.data.campaign_id, this.data.start_date, this.data.end_date);
        let updateStatsContent = this.contentService.updateStats(this.data.campaign_id, this.data.start_date, this.data.end_date);
        Observable.forkJoin(updateStatsCampaign, updateStatsSites, updateStatsContent).subscribe(res => {
            this.messageService.flashSuccess("Update stats successfully!");
            this.showLoading = false;
        }, err => {
            this.messageService.flashError(err);
            this.showLoading = false;
        });
    }

    canSubmit() {
        return this.data.campaign_id && this.data.start_date && this.data.end_date;
    }

    campaignList() {
        this.campaignService.getAllCampaigns().subscribe(res => {
            this.campaigns = _.reverse(res);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    public selectedDate(value: any, dateInput: any) {
        let from = moment(value.start).format(dateFormat);
        let to = moment(value.end).format(dateFormat);
        this.data.start_date = from;
        this.data.end_date = to;
    }

    public calendarEventsHandler(e: any) {

    }

    ngOnDestroy() {

    }
}