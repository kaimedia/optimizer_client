import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from './modals/confirm.component';
import { WidgetService, Site } from '../../shared/services/widgets.service';
import * as _ from 'lodash';

@Component({
    selector: 'dashboard-blacklists',
    templateUrl: './templates/blacklists.component.html',
})

export class BlacklistsComponent implements OnInit, OnDestroy {
    title: string = "Blocked Sites";
    subscription: Subscription;

    trafficSource: string = "all";
    status: string = "all";

    siteList: Array<any> = [];
    siteCount: number = 0;

    filterQuery: string;
    tableSetting = { rowsOnPage: 25, columnCount: 50, sortBy: "updated_at", sortOrder: "desc" };

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private messageService: MessageFlashService,
        private siteService: WidgetService,
    ) { }

    showAllButton: boolean = false;

    ngOnInit() {
        this.list();
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    list() {
        this.subscriptionInit();
        this.subscription = this.siteService.getBlacklist(this.trafficSource, this.status).subscribe(
            response => {
                this.siteList = response;
                this.siteCount = Object.keys(response).length;
            }, err => {
                this.messageService.flashError(err);
            });
    }

    onChangeStatus() {
        this.list();
    }

    onShowCampaigns(item) {
        if (item.showCampaigns) {
            item.showCampaigns = false;
        } else {
            item.showCampaigns = true;
        }
    }

    showAllCampaigns() {
        if (this.showAllButton) {
            this.showAllButton = false;
            this.siteList = _.map(this.siteList, e => {
                e.showCampaigns = false;
                return e;
            });
        } else {
            this.showAllButton = true;
            this.siteList = _.map(this.siteList, e => {
                e.showCampaigns = true;
                return e;
            });
        }
    }

    unBlockSite(item, data) {
        let site = [];
        data.site_id = item.site_id;
        data.trafficSource = item.trafficSource;
        site.push(data);

        this.subscriptionInit();
        this.subscription = this.siteService.manualUnBlockSite({ sites: site }).subscribe(res => {
            this.messageService.flashSuccess(res.message);
            this.list();
        }, err => {
            this.messageService.flashError(err);
        });
    }

    showCalcClicks(x: number, y: number): boolean {
        let subc = this.calcClicks(x, y);
        if ((subc != 0) && !isNaN(subc))
            return true;
        else
            return false;
    }

    displayUnblockButton(item) {
        if (_.includes(['BLOCKED', 'PAUSE'], item.status)) {
            return true;
        }
        return false;
    }

    renderCalcClicks(x: number, y: number): string {
        let subc = this.calcClicks(x, y);
        if (subc > 0)
            return ('+' + subc);
        else
            return subc;
    }

    calcClicks(x: number, y: number): any {
        return x - y;
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}