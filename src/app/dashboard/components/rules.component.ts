import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from './modals/confirm.component';
import { CreateRuleComponent } from './modals/create-rule.component';
import { RuleService } from '../../shared/services/rule.service';
import { element } from 'protractor';
import * as _ from 'lodash';

@Component({
    selector: 'dashboard-rules',
    templateUrl: './templates/rules.component.html',
})

export class RulesComponent implements OnInit, OnDestroy {
    title: string = "Rules";
    @Input() ruleList: Array<any>;
    rulesCount: number = 0;
    filterQuery: string;
    tableSetting = { rowsOnPage: 25, columnCount: 8, sortBy: "created_at", sortOrder: "desc" };
    selectedAll: any;
    ruleActions = [null, 'Email Alert', 'Block', 'Block & Blacklist', 'Increase CPC', 'Decrease CPC'];
    levelInfo = [
        null,
        { icon: 'C', text: 'This rule will be executed on selected campaigns', type: 'success' },
        { icon: 'A', text: 'This rule will be executed on contents of selected campaigns', type: 'info' },
        { icon: 'W', text: 'This rule will be executed on sites of selected campaigns', type: 'primary' },
    ];
    subscription: Subscription;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private ruleService: RuleService,
        private messageService: MessageFlashService,
    ) { }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {
            this.list();
        });
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    list() {
        this.subscriptionInit();
        this.subscription = this.ruleService.list().subscribe(
            rules => {
                this.ruleList = _.filter(rules, e => _.includes(e.campaigns, 'all'));
                this.rulesCount = Object.keys(this.ruleList).length;
            },
            err => {
                this.messageService.flashError(err);
            });
    }

    edit(item) {
        if (!item) return;
        this.openEditForm(item);
    }

    enable(item) {
        if (!item) return;
        this.status(item._id, true);
    }

    disable(item) {
        if (!item) return;

        const title = "Confirm Disable";
        const body = `Are you sure to disable rule: ${item.name}?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.status(item._id, false);
            }
        });
    }

    delete(item) {
        if (!item) return;

        const title = "Confirm Delete";
        const body = `Are you sure to delete rule: ${item.name}?`;
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(title, body);
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.ruleService.remove(item._id).subscribe(res => {
                    this.list();
                }, err => {
                    this.messageService.flashError(err);
                });
            }
        });
    }

    status(id: string, value: boolean) {
        let type: string;
        if (value === true)
            type = "Enable";
        else
            type = "Disable";

        this.ruleService.update(id, { status: value }).subscribe(res => {
            this.list();
            this.messageService.flashSuccess(`${type} rule successfully`);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    openCreateForm() {
        const title = "New Rule";
        const button = "Create";
        this.showContentModal(title, null, button);
    }

    openEditForm(item) {
        const title = `Edit Rule: ${item.name}`;
        const button = "Save";
        this.showContentModal(title, item, button);
    }

    showContentModal(title, body, button) {
        const modal = this.modalService.show(CreateRuleComponent);
        (<CreateRuleComponent>modal.content).showRuleModal(title, body, button);
        (<CreateRuleComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.list();
            }
        });
    }

    showCriterias(item) {
        let criterias: string = '';
        let conditions = item.criterias;

        if (item.action === 4) {
            conditions = item.increase_cpc;
        }

        if (item.action === 5) {
            conditions = item.decrease_cpc;
        }

        conditions.forEach(element => {
            let operator;
            let calc;

            switch (element.operator) {
                case 'greater':
                    operator = '>';
                    break;

                case 'less':
                    operator = '<';
                    break;

                case 'equal':
                    operator = '=';
                    break;
            };

            switch (element.calc) {
                case 'add':
                    calc = '+';
                    break;

                case 'subtract':
                    calc = '-';
                    break;

                case 'multiply':
                    calc = '*';
                    break;

                case 'divide':
                    calc = '/';
                    break;
            };

            let elementValue = element.value;
            if (element.onCalc === true) {
                elementValue = `(${element.value}${calc}${element.xcolumn})`;
            }

            let value = `${elementValue}${element.unit}`;
            if (element.unit === '$') {
                value = `${element.unit}${elementValue}`;
            }

            criterias += `${element.column} ${operator} ${value}, `;
        });

        return criterias.slice(0, -2);
    }

    showLevel(item) {
        return {
            'icon': this.levelInfo[item.level]['icon'],
            'text': this.levelInfo[item.level]['text'],
            'type': this.levelInfo[item.level]['type'],
        };
    }

    showAction(item) {
        return this.ruleActions[item.action];
    }

    isEnalbe(item): boolean {
        if (item.status === true)
            return true;
        return false;
    }

    selectAll() {
        for (var i = 0; i < this.ruleList.length; i++) {
            this.ruleList[i].selected = this.selectedAll;
        }
    }

    checkIfAllSelected() {
        this.selectedAll = this.ruleList.every(function (item: any) {
            return item.selected == true;
        })
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}