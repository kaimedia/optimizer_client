import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NgxPermissionsGuard } from 'ngx-permissions';
import { AccountGuardService } from '../shared/services/accounts.service';
import { DashboardComponent } from './components/dashboard.component';
import { CampaignsComponent } from './components/campaigns.component';
import { WidgetsComponent } from './components/widgets.component';
import { RulesComponent } from './components/rules.component';
import { TrackerComponent } from './components/tracker.component';
import { TrafficSourceComponent } from './components/traffic-source.component';
import { BlacklistsComponent } from './components/blacklists.component';
import { ManualStatsUpdateComponent } from './components/manual-stats-update.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
            title: 'Dashboard',
        }
    },
    {
        path: 'campaign/:id',
        component: WidgetsComponent,
        canActivate: [AccountGuardService],
        data: {
            title: 'Dashboard',
        }
    },
    {
        path: 'rules',
        component: RulesComponent,
        canLoad: [NgxPermissionsGuard],
        data: {
            title: 'Rules',
            permissions: {
                only: 'ADMIN',
                redirectTo: '/dashboard',
            }
        }
    },
    {
        path: 'campaigns',
        component: CampaignsComponent,
        canLoad: [NgxPermissionsGuard],
        data: {
            title: 'Manual Campaign',
            permissions: {
                only: 'ADMIN',
                redirectTo: '/dashboard',
            }
        }
    },
    {
        path: 'blocked-sites',
        component: BlacklistsComponent,
        canLoad: [NgxPermissionsGuard],
        data: {
            title: 'Blocked Sites',
            permissions: {
                only: 'ADMIN',
                redirectTo: '/dashboard',
            }
        }
    },
    {
        path: 'manual-stats-update',
        canLoad: [NgxPermissionsGuard],
        component: ManualStatsUpdateComponent,
        data: {
            title: 'Manual Stats Update',
            permissions: {
                only: 'ADMIN',
                redirectTo: '/dashboard',
            }
        }
    },
    {
        path: 'trackers',
        component: TrackerComponent,
        canLoad: [NgxPermissionsGuard],
        data: {
            title: 'Trackers',
            permissions: {
                only: 'ADMIN',
                redirectTo: '/dashboard',
            }
        }
    },
    {
        path: 'trafficsources',
        component: TrafficSourceComponent,
        canLoad: [NgxPermissionsGuard],
        data: {
            title: 'Traffic Sources',
            permissions: {
                only: 'ADMIN',
                redirectTo: '/dashboard',
            }
        }
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ],
})

export class DashboardRoutingModule { }