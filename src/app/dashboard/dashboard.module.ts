import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from "@angular/http";
import { SharedModule } from '../shared/shared.module';
import { DataTableModule } from 'angular2-datatable';
import { ChartsModule } from 'ng2-charts';
import { DashboardRoutingModule } from './dashboard.routing';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { Select2Module } from 'ng2-select2';
// import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';

import { ConfirmationModalComponent } from './components/modals/confirm.component';
import { CreateRuleComponent } from './components/modals/create-rule.component';
import { CreateTrackerComponent } from './components/modals/create-tracker.component';
import { CreateTrafficSourceComponent } from './components/modals/create-traffic-source.component';
import { UpdateCampaignComponent } from './components/modals/update-campaign.component';
import { SiteBidModalComponent } from './components/modals/update-site-bid.component';
import { BlacklistModalComponent } from './components/modals/blacklist-sites.component';
import { CreateLanderComponent } from './components/modals/create-lander.component';
import { CreateOfferComponent } from './components/modals/create-offer.component';
import { CreateCampaignComponent } from './components/modals/create-campaign.component';

import { Daterangepicker } from 'ng2-daterangepicker';
import { DataFilterPipe } from '../shared/components/data-filter-pipe.component';
import { SortPipe } from "../shared/components/sort-pipe.component";
import { DateRangeComponent } from './components/daterangepicker.component';
import { SystemTimeComponent } from './components/system-time.component';
import { StatsComponent } from './components/stats.component';
import { DashboardComponent } from './components/dashboard.component';
import { CampaignsComponent } from './components/campaigns.component';
import { WidgetsComponent } from './components/widgets.component';
import { SitesComponent } from './components/tabs/sites.component';
import { ContentsComponent } from './components/tabs/contents.component';
import { TrackerViewComponent } from './components/tabs/tracker-view.component';
import { TrackerCampaignComponent } from './components/tabs/tracker-campaign.component';
import { SiteRulesComponent } from './components/tabs/rules.component';
import { RulesComponent } from './components/rules.component';
import { TrackerComponent } from './components/tracker.component';
import { TrafficSourceComponent } from './components/traffic-source.component';
import { BlacklistsComponent } from './components/blacklists.component';
import { ManualStatsUpdateComponent } from './components/manual-stats-update.component';

@NgModule({
    declarations: [
        DateRangeComponent,
        SystemTimeComponent,
        DataFilterPipe,
        SortPipe,
        ConfirmationModalComponent,
        CreateRuleComponent,
        CreateTrackerComponent,
        CreateTrafficSourceComponent,
        UpdateCampaignComponent,
        SiteBidModalComponent,
        BlacklistModalComponent,
        CreateLanderComponent,
        CreateOfferComponent,
        CreateCampaignComponent,
        StatsComponent,
        DashboardComponent,
        CampaignsComponent,
        WidgetsComponent,
        SitesComponent,
        ContentsComponent,
        TrackerCampaignComponent,
        SiteRulesComponent,
        RulesComponent,
        TrackerComponent,
        TrafficSourceComponent,
        TrackerViewComponent,
        BlacklistsComponent,
        ManualStatsUpdateComponent,
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        SharedModule,
        Daterangepicker,
        ChartsModule,
        DashboardRoutingModule,
        DataTableModule,
        MultiselectDropdownModule,
        Select2Module,
        // BootstrapSwitchModule.forRoot(),
    ],
    exports: [
        StatsComponent,
    ],
    providers: [
        FormBuilder,
        DateRangeComponent,
        SystemTimeComponent,
        StatsComponent,
    ],
    entryComponents: [
        CreateRuleComponent,
        CreateTrackerComponent,
        CreateTrafficSourceComponent,
        ConfirmationModalComponent,
        UpdateCampaignComponent,
        SiteBidModalComponent,
        BlacklistModalComponent,
        CreateLanderComponent,
        CreateOfferComponent,
        CreateCampaignComponent,
    ]
})

export class DashboardModule { }