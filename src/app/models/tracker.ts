export class Tracker {
    _id: string;
    user_id: string;
    name: string;
    type: number;
    access_id: string;
    access_key: string;
    status: boolean;
    created_at: Date;
    updated_at: Date;
}