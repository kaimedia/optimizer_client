export class Site {
    _id: string;
    site_id: string;
    site_name: string;
    trafficSource: string;
    metrics: Array<any>;
    status: string;
    created_at: Date;
    updated_at: Date;
}