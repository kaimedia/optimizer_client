export class User {
	_id: string;
	email: string;
	password: string;
	newPassword: string;
	passwordConfirmation: string;
	firstName: string;
	lastName: string;
	avatar: string;
	address: string;
	country: string;
	token: string;
	role: string;
	active: boolean;
	created_at: Date;
	updated_at: Date;
}
