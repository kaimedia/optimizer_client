export class Rule {
    _id: string;
    name: string;
    action: number;
    interval: string;
    rotation: string;
    level: number;
    campaigns: Array<any>;
    criterias: Object;
    status: boolean;
    created_at: Date;
    updated_at: Date;
}