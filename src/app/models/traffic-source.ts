export class TrafficSource {
    _id: string;
    user_id: string;
    name: string;
    type: string;
    account_id: string;
    client_id: string;
    client_secret: string;
    username: string;
    email: string;
    password: string;
    trackers: Array<any>;
    status: boolean;
    created_at: Date;
    updated_at: Date;
}