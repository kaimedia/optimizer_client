export class Campaign {
    _id: string;
    id: string;
    name: string;
    cpc: number;
    spending_limit: number;
    daily_cap: number;
    daily_ad_delivery_model: string;
    publisher_targeting: object;
    publisher_bid_modifier: object;
    start_date: string;
    type: string;
    countryCode: string;
    trafficSourceAccount: string;
    trackerAccount: string;
    tracker_id: string;
    id_fraud: string;
    campaignUrl: string;
    deployUrl: string;
    payout: number;
    is_active: string;
    status: string;
    archived: number;
    created_at: Date;
    updated_at: Date;
}