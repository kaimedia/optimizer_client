import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CoreModule } from './core/core.module';
import { AppComponent } from './app.component';
import { AppRouting } from './app-routing.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { SharedModule } from './shared/shared.module';
import { PageLoadingService } from './shared/services/page-loading.service';
import { MessageFlashService } from './shared/services/message-flash.service';
import { DashboardModule } from './dashboard/dashboard.module';
import { LayoutModule } from './layout/layout.module';
import { UserModule } from './user/user.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CommonModule,
    CoreModule,
    BrowserAnimationsModule,
    AuthenticationModule,
    SharedModule,
    DashboardModule,
    LayoutModule,
    UserModule,
    AppRouting,
  ],
  providers: [
    PageLoadingService,
    MessageFlashService,
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
