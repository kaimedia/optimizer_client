import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticatedComponent } from './layout/components/authenticated.component';
import { SelectivePreloadingStrategy } from './core/strategies/selective-preload.strategy';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AuthGuardService } from './shared/services/auth-guard.service';
import { UnAuthGuardService } from './shared/services/un-auth-guard.service';
import { ErrorHandlerComponent } from './shared/components/error-handler.component';
import { UserResolver } from './shared/resolves/user.resolver';

const routes: Routes = [
    {
        path: '',
        component: AuthenticatedComponent,
        canActivate: [AuthGuardService],
        canActivateChild: [NgxPermissionsGuard],
        children: [
            {
                path: '',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule',
            },
            {
                path: 'user',
                loadChildren: 'app/user/user.module#UserModule',
                data: {
                    permissions: {
                        only: ['ADMIN', 'admin'],
                        redirectTo: '/dashboard',
                    }
                }
            }
        ],
        resolve: {
            user: UserResolver
        },
        data: {
            preload: true
        }
    },
    {
        path: 'auth',
        loadChildren: 'app/authentication/authentication.module#AuthenticationModule',
        canActivate: [UnAuthGuardService]
    },
    {
        path: 'error',
        component: ErrorHandlerComponent,
    },
    {
        path: 'error/:code',
        component: ErrorHandlerComponent,
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule,
    ],
    providers: [
        SelectivePreloadingStrategy,
    ]
})

export class AppRouting { }