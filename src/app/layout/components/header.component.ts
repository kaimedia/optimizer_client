import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from "../../shared/services/user.service";
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'layout-header',
    templateUrl: './templates/header.component.html'
})
export class HeaderComponent {
    user;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private userService: UserService,
        private messageService: MessageFlashService,
    ) { }

    ngOnInit() {
        this.activatedRoute.data.subscribe((data: { user }) => {
            this.user = data.user;
        });
    }

    logout() {
        this.userService.logout();
    }

    changePassword() {
        this.router.navigateByUrl('/user/change-password');
    }

}