import { Component, OnInit } from '@angular/core';
import { UserService, User } from '../../shared/services/user.service';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { Router } from '@angular/router';
import { StorageService } from "../../core/services/storage.service";
import { RegexHelper } from '../../shared/helpers/regex.helper';

@Component({
    selector: 'auth-register',
    templateUrl: './templates/register.component.html',
})

export class RegisterComponent implements OnInit {
    title: String = "Sign Up";
    user: User = new User();
    submitted: boolean = false;
    registerSuccess: boolean;
    regexHelper = RegexHelper;

    constructor(
        private router: Router,
        private storageService: StorageService,
        private userService: UserService,
        private messageService: MessageFlashService,
    ) { }

    ngOnInit() {

    }

    onSubmit() {
        this.submitted = true;
        if (!this.canSubmit()) {
            return;
        }

        delete this.user['passwordConfirmation'];
        return this.userService.register(this.user, this.callback.bind(this));
    }

    callback(errMsg, response) {
        if (errMsg) {
            this.messageService.flashError(errMsg);
            return;
        }
        this.title = 'Create user successfully.';
        this.registerSuccess = true;
        this.router.navigateByUrl('/auth/login');
    }

    canSubmit() {
        return this.user.firstName && this.user.lastName && this.user.email && this.user.password && this.user.passwordConfirmation && this.user.password === this.user.passwordConfirmation;
    }
}