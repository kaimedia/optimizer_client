import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { AuthenticationComponent } from './components/authentication.component';
import { LoginComponent } from './components/login.component';
import { RegisterComponent } from './components/register.component';
import { ForgotPasswordComponent } from './components/fogot-password.component';
import { AuthenticationRoutingModule } from './authentication.routing';

@NgModule({
    declarations: [
        AuthenticationComponent,
        LoginComponent,
        RegisterComponent,
        ForgotPasswordComponent,
    ],
    imports: [
        FormsModule,
        CommonModule,
        SharedModule,
        AuthenticationRoutingModule,
    ],
    exports: [],
    providers: [],
})

export class AuthenticationModule { }