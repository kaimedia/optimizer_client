var UIDatepaginator = function () {

    return {

        //main function to initiate the module
        init: function () {

            //sample #1
            $('#datepaginator').datepaginator({
                // size: "large",
                // selectedDateFormat: "YYYY-MM-DD",
                endDate: moment(new Date()),
                onSelectedDateChanged: function (event, date) {
                    alert("Selected date: " + moment(date).format("MM/DD/YYYY"));
                }
            });

        } // end init

    };

}();